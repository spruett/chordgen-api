build:
	./mvnw package
docker: build
	./mvnw jib:dockerBuild
push: docker
	docker push registry.gitlab.com/spruett/chordgen-api
