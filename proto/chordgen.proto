syntax = "proto3";

package chordgen;

option java_package = "xyz.chordgen.proto";
option java_multiple_files = true;
option java_outer_classname = "Proto";

// Common library types in their protocol form, some information
// is lost or canonicalized in the name. For example, we currently
// do not pass the parsed note name, just the string form (e.g. B#).

enum NoteType {
    ROOT = 0;
    THIRD = 1;
    FIFTH = 2;
    SEVENTH = 3;

    EXTENSION = 4;

    BASS = 5;
    MELODY = 6;
}
message Interval {
    int32 range = 1;
    int32 sharps = 2;
    int32 flats = 3;
}
message Note {
    string name = 1;
    NoteType type = 2;
    Interval interval = 3;
}
message PlayedNote {
    Note note = 1;
    GuitarString string = 2;
    int32 fret = 3;
    int32 finger = 4;
    double pitch = 5;
}

enum ChordFlavor {
    MAJOR = 0;
    MINOR = 1;
    DIMINISHED = 2;
    AUGMENTED = 3;

    DOMINANT = 4;
    MAJOR_SEVEN = 5;
    SIXTH = 6;

    EXTENSIONS = 20;
}
message Chord {
    // Chord is canonical on its name, rest is display information
    string name = 1;

    repeated Note notes = 2;
    repeated ChordFlavor flavors = 3;
}

message GuitarString {
    string name = 1;
    int32 index = 2;
}
enum TuningId {
    STANDARD = 0;
    DROP_D = 1;
    UKULELE = 2;

    DOUBLE_DROP_D = 3;
    DADGAD = 4;
    OPEN_D = 5;
    OPEN_E = 6;
    OPEN_G = 7;
    OPEN_A = 8;
}
message Tuning {
    // First two describe the full tuning, but string list
    // is included for client rendering
    TuningId id = 1;
    int32 capo = 2;
    repeated GuitarString strings = 3;
}

message Voicing {
    Chord chord = 1;
    repeated PlayedNote notes = 2;
    Tuning tuning = 3;
}
message ScoringInformation {
    double score = 1;
    map<string, double> subscores = 2;
}
message Settings {
    TuningId tuning = 1;
    int32 capo = 2;
    int32 targetPosition = 3;
    int32 targetFret = 4;
    bool disableDedup = 5;
    bool disableBars = 6;
}

// Composite types for actual messages
message NoteList {
    repeated PlayedNote notes = 1;
}
message VoicingSet {
    Chord chord = 1;
    Tuning tuning = 2;
    repeated NoteList voicings = 3;
}
message AllChordVoicings {
    map<string, VoicingSet> voicings = 1;
    map<string, string> aliases = 2;
}

enum ErrorCode {
    NONE = 0;
    NOTE_PARSE_INVALID = 1;
    NOTE_PARSE_SHARP_FLAT = 2;
    PARSE_EMPTY = 3;
    CHORD_NAME_INVALID = 4;
    CHORD_SUFFIX_INVALID = 5;
}
// Generic error type
message Error {
    string message = 1;
    ErrorCode code = 2;
}