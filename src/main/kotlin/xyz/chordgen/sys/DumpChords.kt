package xyz.chordgen.sys

import com.google.protobuf.util.JsonFormat
import xyz.chordgen.lib.Chord
import xyz.chordgen.lib.Fingering
import xyz.chordgen.lib.ScoringOptions
import xyz.chordgen.lib.Tunings
import xyz.chordgen.lib.getChordFingerings
import xyz.chordgen.lib.sharpsFlatsToUnicode
import xyz.chordgen.lib.translateFingerings
import xyz.chordgen.proto.AllChordVoicings
import xyz.chordgen.proto.VoicingSet
import java.nio.file.Files
import java.nio.file.Paths

fun generateBestFingerings(chordName: String): List<Fingering> {
    val chord = Chord.fromString(chordName)
    return getChordFingerings(
            chord,
            tuning = Tunings.standardTuning,
            opts = ScoringOptions(),
            maxResults = 20
    ).map {
        it.first
    }
}

/*
 * Generates a dump of common chord names to their top fingerings, in protobuf (or JSON format).
 *
 * First command line argument is the path to the output file. If it ends with .json, output
 * is formatted as JSON, else it is protobuf (see AllChordVoicings in proto/chordgen.proto)
 */
fun main(args: Array<String>) {
    val outputFilename = Paths.get(args[0])
    val notes = "ABCDEFG".toList().map { it.toString() }
    val noteSuffixes = listOf("", "b", "#")
    val chordSuffixes = listOf("", "m", "dim", "aug", "dim7", "7", "9", "m7", "M7", "add9", "sus4", "11")

    val chordSuffixAliases = mapOf(
            "add9" to "sus2",
            "aug" to "+"
    )

    val bestByName = mutableMapOf<String, VoicingSet>()
    val aliases = mutableMapOf<String, String>()
    var idx = 1
    for (note in notes) {
        for (noteSuffix in noteSuffixes) {
            for (chordSuffix in chordSuffixes) {
                val chordName = "$note$noteSuffix$chordSuffix"
                val fingerings = generateBestFingerings(chordName)

                bestByName[chordName] = translateFingerings(
                    Chord.fromString(chordName),
                    Tunings.standardTuning,
                    fingerings
                )
                aliases[sharpsFlatsToUnicode(chordName)] = chordName
                println("[$idx/ ${notes.size * noteSuffixes.size * chordSuffixes.size}] Finished: $chordName")
                idx += 1
            }
            // chord suffix aliases
            for ((value, alias) in chordSuffixAliases) {
                aliases["$note$noteSuffix$alias"] = "$note$noteSuffix$value"
            }
        }
    }
    val allVoicings = AllChordVoicings.newBuilder()
            .putAllVoicings(bestByName)
            .build()

    val serialized = if (outputFilename.toString().endsWith(".json")) {
        JsonFormat.printer().print(allVoicings).toByteArray()
    } else {
        allVoicings.toByteArray()
    }
    Files.write(outputFilename, serialized)
}