package xyz.chordgen.api

import com.google.protobuf.Message
import com.google.protobuf.util.JsonFormat
import org.jooby.Kooby
import org.jooby.MediaType
import org.jooby.Request
import org.jooby.handlers.Cors
import org.jooby.handlers.CorsHandler
import org.jooby.run
import xyz.chordgen.lib.Chord
import xyz.chordgen.lib.UserException
import xyz.chordgen.lib.getChordFingerings
import xyz.chordgen.lib.settingsToOptions
import xyz.chordgen.lib.settingsToTuning
import xyz.chordgen.lib.translateChord
import xyz.chordgen.lib.translateFingerings
import xyz.chordgen.proto.Error
import xyz.chordgen.proto.ErrorCode
import xyz.chordgen.proto.Settings
import java.util.Base64

private fun Request.parseSettings(): Settings {
    val settingsParam = param("settings")
    if (!settingsParam.isSet) {
        return Settings.newBuilder().build()
    }
    return Settings.parseFrom(
        Base64.getDecoder().decode(settingsParam.value())
    )
}

class ApiServer : Kooby({
    use("*", CorsHandler(Cors()))
    renderer { value, ctx ->
        if (value is Message) {
            val protobuf = MediaType.valueOf("application/x-protobuf")
            val serialized = if (ctx.accepts(protobuf)) {
                ctx.type(protobuf)
                value.toByteArray()
            } else {
                ctx.type(MediaType.json)
                val printer = JsonFormat.printer()
                    .includingDefaultValueFields()
                    .omittingInsignificantWhitespace()
                printer.print(value).toByteArray()
            }
            ctx.send(serialized)
        }
    }
    err(Exception::class.java) { _, resp, err ->
        var code = ErrorCode.NONE
        val cause = err.cause
        if (cause is UserException) {
            resp.status(400)
            code = cause.code
        } else {
            resp.status(500)
        }
        val errorMessage = Error.newBuilder()
            .setMessage(err.cause?.message)
            .setCode(code)
            .build()
        resp.send(errorMessage)
    }

    get("/voicings/") {
        val chordName = param("name").value()
        val chord = Chord.fromString(chordName)
        val settings = parseSettings()

        val tuning = settingsToTuning(settings)
        val opts = settingsToOptions(settings)
        val voicings = getChordFingerings(
            chord = chord,
            tuning = tuning,
            opts = opts,
            maxResults = 20
        )
        translateFingerings(chord, tuning, voicings.map { it.first })
    }
    get("/notes/") {
        val chord = Chord.fromString(param("name").value())
        translateChord(chord)
    }
})

fun main(args: Array<String>) {
    run({ ApiServer() }, *args)
}