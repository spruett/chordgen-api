package xyz.chordgen.lib

import xyz.chordgen.proto.ErrorCode

/*
 * Overall chord "flavor" (major/minor/...) which determines the third and fifth
 * of the base triad. Extensions (major-7, sus4) are separate, see ChordExtension.
 */
enum class ChordFlavor {
    DIMINISHED,
    MINOR,
    MAJOR,
    AUGMENTED
}

/*
 * An extension to a triad: an additional note. Not exclusive, some chords
 * may have multiple extensions (e.g. Cm7b9 -> {SEVEN, FLAT_NINE}).
 *
 * Each extension is a fixed interval from the root note of the chord.
 */
enum class ChordExtension {
    SUS2,
    SUS4,
    SIXTH,
    SEVEN,
    MAJOR_SEVEN,
    FLAT_NINE,
    NINE,
    SHARP_NINE,
    ELEVEN,
    THIRTEEN;

    /*
     * Gives the interval from this extension to the root of the chord as a delta.
     */
    fun toInterval(): Interval {
        return when (this) {
            SUS2 -> Interval(2)
            SUS4 -> Interval(4)
            SIXTH -> Interval(6)
            SEVEN -> Interval(7, flats = 1)
            MAJOR_SEVEN -> Interval(7)
            FLAT_NINE -> Interval(9, flats = 1)
            NINE -> Interval(9)
            SHARP_NINE -> Interval(9, sharps = 1)
            ELEVEN -> Interval(11)
            THIRTEEN -> Interval(13)
        }
    }
}

/*
 * Classification of each note in a chord. Typical triads will have ROOT, THIRD, FIFTH.
 * Sevenths are handled specially. See ChordExtension above.
 */
enum class ChordNoteType {
    // A base note, such as D/G. May or not be a part of the actual chord.
    BASS,
    ROOT,
    THIRD,
    FIFTH,
    SEVENTH,
    // Any non-seventh extension, see ChordExtension
    EXTENSION,
    // A melody note on top of a chord, like "F#" on top of a C chord (denoted F#\C)
    MELODY
}

/*
 * An individual note in a chord with classification, useful for scoring
 * placements of notes in a voicing.
 */
data class ChordNote(
    val note: Note,
    val type: ChordNoteType,
    // Was the chord explicit in describing this note. For example,
    // an 11 chord has a 7 and a 9, but those are not explicit (while the 11 is).
    // Useful for scoring voicings when there are too many notes to fit
    val explicit: Boolean = false
)

/*
 * Logical chord structure built on individual notes. This is the
 * chord spelling in the music theory sense, it is not mapped to
 * any physical pitches (or frets).
 *
 * The chord is stored in a canonical form as a list of ChordNotes
 * in a canonical order (explicit bass if necessary, root, third, fifth, ...).
 */
data class Chord(
    private val root: Note,
    private val flavor: ChordFlavor,
    private val extensions: Set<ChordExtension> = setOf(),
    private val bassNote: Note? = null,
    private val melodyNote: Note? = null
) {
    val notes: List<ChordNote> = buildNotes()

    private fun buildNotes(): List<ChordNote> {
        return listOfNotNull(getBass()) +
            listOfNotNull(getMelody()) +
            listOf(getRoot()) +
            listOfNotNull(getThird()) +
            listOfNotNull(getFifth()) +
            getExtensions()
    }

    fun getFlavor(): ChordFlavor {
        return flavor
    }

    fun getBass(): ChordNote? {
        return bassNote?.let {
            ChordNote(it, ChordNoteType.BASS, explicit = true)
        }
    }

    fun getMelody(): ChordNote? {
        return melodyNote?.let {
            ChordNote(it, ChordNoteType.MELODY, explicit = true)
        }
    }

    fun getRoot(): ChordNote {
        return ChordNote(root, ChordNoteType.ROOT)
    }

    fun getThird(): ChordNote {
        val note = root.addInterval(
            when (flavor) {
                ChordFlavor.MINOR, ChordFlavor.DIMINISHED -> Interval(3, flats = 1)
                else -> Interval(3)
            }
        )
        return ChordNote(note, ChordNoteType.THIRD)
    }

    fun getFifth(): ChordNote {
        val note = root.addInterval(
            when (flavor) {
                ChordFlavor.DIMINISHED -> Interval(5, flats = 1)
                ChordFlavor.AUGMENTED -> Interval(5, sharps = 1)
                else -> Interval(5)
            }
        )
        return ChordNote(note, ChordNoteType.FIFTH, explicit = false)
    }

    fun getExtensions(): List<ChordNote> {
        val sevenths = setOf(ChordExtension.SEVEN, ChordExtension.MAJOR_SEVEN)
        val ninths = setOf(ChordExtension.NINE, ChordExtension.FLAT_NINE, ChordExtension.SHARP_NINE)
        return listOfNotNull(
            getExtensionOrImplicit(
                sevenths,
                ChordNoteType.SEVENTH,
                ChordExtension.SEVEN,
                ninths + setOf(ChordExtension.ELEVEN, ChordExtension.THIRTEEN)
            ),
            getExtensionOrImplicit(
                ninths,
                ChordNoteType.EXTENSION,
                ChordExtension.NINE,
                setOf(ChordExtension.ELEVEN, ChordExtension.THIRTEEN)
            ),
            getExtensionOrImplicit(
                setOf(ChordExtension.ELEVEN),
                ChordNoteType.EXTENSION,
                ChordExtension.ELEVEN,
                setOf(ChordExtension.THIRTEEN)
            ),
            getExtensionOrImplicit(
                setOf(ChordExtension.THIRTEEN)
            ),
            getExtensionNote(setOf(ChordExtension.SUS2)),
            getExtensionNote(setOf(ChordExtension.SUS4)),
            getExtensionNote(setOf(ChordExtension.SIXTH))
        )
    }

    private fun getExtensionOrImplicit(
        set: Set<ChordExtension>,
        type: ChordNoteType = ChordNoteType.EXTENSION,
        implicit: ChordExtension = set.first(),
        ifAnyExplicit: Set<ChordExtension> = setOf()
    ): ChordNote? {
        val explicit = getExtensionFrom(set)
        if (explicit != null) {
            return ChordNote(root.addInterval(explicit.toInterval()), type, explicit = true)
        }
        if (extensions.any { ifAnyExplicit.contains(it) }) {
            return ChordNote(root.addInterval(implicit.toInterval()), type, explicit = false)
        }
        return null
    }

    private fun getExtensionNote(set: Set<ChordExtension>, type: ChordNoteType = ChordNoteType.EXTENSION): ChordNote? {
        val extension = getExtensionFrom(set)
        return extension?.let {
            return ChordNote(root.addInterval(it.toInterval()), type, explicit = true)
        }
    }

    private fun getExtensionFrom(set: Set<ChordExtension>): ChordExtension? {
        val matched = extensions.count { set.contains(it) }
        if (matched == 0) {
            return null
        } else if (matched > 1) {
            throw IllegalStateException("Multiple extensions of the same type: $set")
        }
        return extensions.first { set.contains(it) }
    }

    /*
     * Gets a chord note that matches a physical pitch, if one exists in this chord.
     * Returns null if no notes in the chord match the given pitch.
     */
    fun noteFromPitch(pitch: Pitch): ChordNote? {
        return notes.find {
            it.note.toSemitoneOffset() == pitch.semitoneOffset()
        }
    }

    override fun toString(): String {
        val name = root.toString()
        val flavor = when (flavor) {
            ChordFlavor.AUGMENTED -> "+"
            ChordFlavor.DIMINISHED -> "dim"
            ChordFlavor.MAJOR -> ""
            ChordFlavor.MINOR -> "m"
        }
        val exts = extensions.joinToString("") {
            when (it) {
                ChordExtension.SUS2 -> "sus2"
                ChordExtension.SUS4 -> "sus4"
                ChordExtension.SIXTH -> "6"
                ChordExtension.SEVEN -> "7"
                ChordExtension.MAJOR_SEVEN -> "M7"
                ChordExtension.FLAT_NINE -> "♭9"
                ChordExtension.NINE -> "9"
                ChordExtension.SHARP_NINE -> "♯9"
                ChordExtension.ELEVEN -> "11"
                ChordExtension.THIRTEEN -> "13"
            }
        }
        return "$name$flavor$exts"
    }

    companion object {
        /*
         * Parses a chord from common string notation, e.g. "C" or "GbM7" (Gb with a major-7).
         *
         * Throws if the input is not well specified.
         */
        fun fromString(rawInput: String): Chord {
            if (rawInput.isEmpty()) {
                throw UserException("Chord name cannot be empty", ErrorCode.PARSE_EMPTY)
            }
            val input = sharpsFlatsToAscii(rawInput)
            val melody = takeRegex("[A-G](#|b)*\\\\", input)
            val melodyNote = melody?.let {
                Note.fromString(it.first.dropLast(1))
            }
            val name = melody?.second ?: input
            val match = takeRegex("[A-G](#|b)*", name)
                ?: throw UserException("Invalid chord name: $name", ErrorCode.CHORD_NAME_INVALID)

            val (note, remaining) = match
            val (flavor, extensions) = takeRegex("(dim|m|\\+|aug|)", remaining)
                ?: throw UserException("Invalid chord suffix: $remaining", ErrorCode.CHORD_SUFFIX_INVALID)
            val parsedFlavor = when (flavor) {
                "dim" -> ChordFlavor.DIMINISHED
                "m" -> ChordFlavor.MINOR
                "+", "aug" -> ChordFlavor.AUGMENTED
                else -> ChordFlavor.MAJOR
            }

            var remainingExtensions = extensions
            val parsedExtensions = mutableSetOf<ChordExtension>()
            while (remainingExtensions.isNotEmpty() && !remainingExtensions.startsWith("/")) {
                val extMatch = takeRegex(
                    "6|7|M7|flat9|b9|9|sharp9|#9|add9|11|13|sus2|sus4",
                    remainingExtensions
                )
                    ?: throw UserException(
                        "Invalid chord extension: $remainingExtensions",
                        ErrorCode.CHORD_SUFFIX_INVALID
                    )
                remainingExtensions = extMatch.second

                val parsed = when (extMatch.first) {
                    "6" -> ChordExtension.SIXTH
                    "7" -> ChordExtension.SEVEN
                    "M7" -> ChordExtension.MAJOR_SEVEN
                    "flat9", "b9" -> ChordExtension.FLAT_NINE
                    "9" -> ChordExtension.NINE
                    "sharp9", "#9" -> ChordExtension.SHARP_NINE
                    "11" -> ChordExtension.ELEVEN
                    "13" -> ChordExtension.THIRTEEN
                    "add9", "sus2" -> ChordExtension.SUS2
                    "sus4" -> ChordExtension.SUS4
                    else -> throw UserException(
                        "Failed to parse extension: ${extMatch.first}",
                        ErrorCode.CHORD_SUFFIX_INVALID
                    )
                }
                parsedExtensions.add(parsed)
            }
            var bassNote: Note? = null
            if (remainingExtensions.startsWith("/")) {
                bassNote = Note.fromString(remainingExtensions.drop(1))
            }
            return Chord(Note.fromString(note), parsedFlavor, parsedExtensions, bassNote, melodyNote)
        }

        private fun takeRegex(pattern: String, haystack: String): Pair<String, String>? {
            val regex = Regex("^($pattern).*")
            val match = regex.find(haystack)
            return match?.let {
                val found = it.groupValues.drop(1).first()
                val remaining = haystack.removePrefix(found)
                return Pair(found, remaining)
            }
        }
    }
}