package xyz.chordgen.lib

import xyz.chordgen.proto.Settings
import xyz.chordgen.proto.TuningId

data class Tuning(
    val id: TuningId,
    val strings: List<GuitarString>
) {
    fun withCapo(capo: Int): Tuning {
        return copy(
            strings = strings.map { it.copy(frets = capo..it.frets.last) }
        )
    }
}

fun settingsToTuning(settings: Settings): Tuning {
    val baseTuning = when (settings.tuning) {
        TuningId.STANDARD -> Tunings.standardTuning
        TuningId.UKULELE -> Tunings.ukuleleStandard
        TuningId.DROP_D -> Tunings.dropDTuning
        TuningId.DOUBLE_DROP_D -> Tunings.doubleDropDTuning
        TuningId.DADGAD -> Tunings.dadgadTuning
        TuningId.OPEN_D -> Tunings.openDTuning
        TuningId.OPEN_E -> Tunings.openETuning
        TuningId.OPEN_G -> Tunings.openGTuning
        TuningId.OPEN_A -> Tunings.openATuning
        null, TuningId.UNRECOGNIZED -> throw UserException(
            "Unrecognized tuning information, maybe client is out of date"
        )
    }
    return baseTuning.withCapo(settings.capo)
}

private fun makeTuning(
    id: TuningId,
    stringPitches: List<Pair<String, Int>>,
    maxFret: Int = 12
): Tuning {
    return Tuning(
        id = id,
        strings = stringPitches.mapIndexed { index, (note, octave) ->
            GuitarString(note, Pitch.fromNoteAndOctave(Note.fromString(note), octave), 0..maxFret, index)
        }
    )
}

/*
 * Predefined "tunings", or configured sets of guitar strings with their
 * open pitches and names.
 */
object Tunings {
    val standardTuning = makeTuning(
        TuningId.STANDARD,
        listOf(
            "E" to 2,
            "A" to 2,
            "D" to 3,
            "G" to 3,
            "B" to 3,
            "E" to 4
        )
    )
    val dropDTuning = makeTuning(
        TuningId.DROP_D,
        listOf(
            "D" to 2,
            "A" to 2,
            "D" to 3,
            "G" to 3,
            "B" to 3,
            "E" to 4
        )
    )
    val doubleDropDTuning = makeTuning(
        TuningId.DOUBLE_DROP_D,
        listOf(
            "D" to 2,
            "A" to 2,
            "D" to 3,
            "G" to 3,
            "B" to 3,
            "D" to 4
        )
    )
    val dadgadTuning = makeTuning(
        TuningId.DADGAD,
        listOf(
            "D" to 2,
            "A" to 2,
            "D" to 3,
            "G" to 3,
            "A" to 3,
            "D" to 4
        )
    )
    val openDTuning = makeTuning(
        TuningId.OPEN_D,
        listOf(
            "D" to 2,
            "A" to 2,
            "D" to 3,
            "F#" to 3,
            "A" to 3,
            "D" to 4
        )
    )
    val openETuning = makeTuning(
        TuningId.OPEN_E,
        listOf(
            "E" to 2,
            "B" to 2,
            "E" to 3,
            "G#" to 3,
            "B" to 3,
            "E" to 4
        )
    )
    val openGTuning = makeTuning(
        TuningId.OPEN_G,
        listOf(
            "D" to 2,
            "G" to 2,
            "D" to 3,
            "G" to 3,
            "B" to 3,
            "D" to 4
        )
    )
    val openATuning = makeTuning(
        TuningId.OPEN_A,
        listOf(
            "E" to 2,
            "A" to 2,
            "E" to 3,
            "A" to 3,
            "C#" to 3,
            "E" to 4
        )
    )
    val ukuleleStandard = makeTuning(
        TuningId.UKULELE,
        listOf(
            "G" to 4,
            "C" to 4,
            "E" to 4,
            "A" to 4
        ),
        // Most people don't play uke up to fret 12
        maxFret = 8
    )
}