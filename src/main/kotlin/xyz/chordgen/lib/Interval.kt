package xyz.chordgen.lib

/*
 * Interval (delta between two pitches) in the music theory sense.
 *
 * `range` is the typical numeral used when referring to a major/perfect
 * interval, e.g. "3" for a major third or "5" for a perfect fifth.
 *
 * Flats/sharps are stored as counters. For example, Interval(3, flats=1)
 * is a minor 3rd (single fat on a major 3rd).
 */
data class Interval(val range: Int, val flats: Int = 0, val sharps: Int = 0) {
    init {
        // Either flats or sharps, not both
        check(flats == 0 || sharps == 0)
    }

    /*
     * Converts this interval into an absolute number of semitones,
     * useful for offsetting from a base pitch. For example, a perfect
     * fifth is 7 semitones.
     */
    fun toSemitoneOffset(): Int {
        val rangeOffset = when (normalizeInterval(range - 1) + 1) {
            1 -> 0
            2 -> 2
            3 -> 4
            4 -> 5
            5 -> 7
            6 -> 9
            7 -> 11
            else -> throw IllegalStateException("Invalid interval value: $range")
        }
        return rangeOffset + sharps - flats
    }

    companion object {
        fun fromRangeAndSemitoneOffset(range: Int, semitoneOffset: Int): Interval {
            val base = Interval(range)
            val semitoneDiff = semitoneOffset - base.toSemitoneOffset()
            if (semitoneDiff > 0) {
                return Interval(range, sharps = semitoneDiff)
            }
            return Interval(range, flats = -semitoneDiff)
        }
    }
}