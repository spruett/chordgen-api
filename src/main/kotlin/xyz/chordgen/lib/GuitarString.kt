package xyz.chordgen.lib

data class GuitarString(
    val name: String,
    val openPitch: Pitch,
    val frets: IntRange = 0..15,
    val index: Int
) {
    fun fretPitch(fret: Int): Pitch {
        return Pitch(openPitch.semitone + fret)
    }
}