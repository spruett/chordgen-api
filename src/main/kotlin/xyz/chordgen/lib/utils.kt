package xyz.chordgen.lib

/*
 * "Normalize"s a number to be between 0 and `max`,
 * wrapping around.
 */
fun normalize(offset: Int, max: Int): Int {
    var normalized = offset
    while (normalized < 0) {
        normalized += max
    }
    return normalized % max
}

/*
 * Normalizes a semitone offset (within an octave), between 0 and 12
 */
fun normalizeSemitoneOffset(offset: Int): Int {
    return normalize(offset, 12)
}

/*
 * Normalizes an interval, between 0 and 7.
 */
fun normalizeInterval(offset: Int): Int {
    return normalize(offset, 7)
}

/*
 * Converts all unicode sharp/flat signs in the string
 * to their ASCII equivalents (# / b).
 */
fun sharpsFlatsToAscii(name: String): String {
    return name.replace("♯", "#")
            .replace("♭", "b")
}

/*
 * Inverts of sharpsFlatsToAscii: converts `b` and `#`
 * to unicode sharp/flat symbols.
 */
fun sharpsFlatsToUnicode(name: String): String {
    return name.replace("#", "♯")
            .replace("b", "♭")
}
