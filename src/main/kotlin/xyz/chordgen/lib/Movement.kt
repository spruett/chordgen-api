package xyz.chordgen.lib

import xyz.chordgen.lib.util.BoundedSolutionList
import kotlin.math.roundToInt

data class MovementScoringOptions(
        // penalty per fret of position shift squared
    val positionShiftSquaredPenalty: Double = -2.8,
    val positionShiftPenalty: Int = -1,
        // penalty per string (up/down) per finger moved
    val stringMovementPenalty: Int = -1,
        // penalty per fret per finger moved (other than first)
    val fretMovementPenalty: Int = -2,
        // penalty to add a finger not used
    val fingerAddPenalty: Int = -2,
    val fingerRemovePenalty: Int = -1,
        // extra weight on total score if two base chords are the same
    val bonusWeightForSameChord: Int = 10
)

private fun getFingerPositions(fingering: Fingering): List<Pair<Finger, Pair<Int, Int>>> {
    val comparator = compareBy<Pair<Finger, Pair<Int, Int>>> { it.first }
            .thenBy { -it.second.second }
    return fingering.fingers.toList()
            .filter { !it.first.isOpen() }
            .map {
                it.second to (it.first.fret to it.first.string.index)
            }
            .sortedWith(comparator)
            .distinctBy { it.first }
}

private fun scoreMovement(from: Fingering, to: Fingering, opts: MovementScoringOptions): ScoringInformation {
    val score = ScoringInformation("movement")

    val fromPositions = getFingerPositions(from)
    val toPositions = getFingerPositions(to)

    val fromPosition = fromPositions.firstOrNull()?.second?.first ?: 0
    val toPosition = toPositions.firstOrNull()?.second?.first ?: 0
    val positionDiff = Math.abs(toPosition - fromPosition)
    score.add("shift_sq", (positionDiff * positionDiff * opts.positionShiftSquaredPenalty).roundToInt())
    score.add("shift", positionDiff * opts.positionShiftPenalty)

    val fromMap = fromPositions.toMap()
    val toMap = toPositions.toMap()
    var stringMoveScore = 0
    var fretMoveScore = 0
    var first = true
    for ((finger, loc) in fromPositions) {
        val (fret, string) = loc

        val to = toMap[finger]
        if (to != null) {
            val (toFret, toString) = to
            stringMoveScore += Math.abs(string - toString) * opts.stringMovementPenalty
            if (!first) {
                fretMoveScore += Math.abs(fret - toFret) * opts.fretMovementPenalty
            }
        }
        first = false
    }
    score.add("string", stringMoveScore)
    score.add("fret", fretMoveScore)
    var added = 0
    for (finger in toMap.keys) {
        if (!fromMap.containsKey(finger)) {
            added += opts.fingerAddPenalty
        }
    }
    score.add("add", added)
    var removed = 0
    for (finger in fromMap.keys) {
        if (!toMap.containsKey(finger)) {
            removed += opts.fingerRemovePenalty
        }
    }
    score.add("remove", removed)

    if (from.voicing.chord.getRoot().note == to.voicing.chord.getRoot().note) {
        score.subclasses["movement.same_chord"] = opts.bonusWeightForSameChord
        score.scale(opts.bonusWeightForSameChord)
    }
    return score
}

data class VoiceLeading(
    val chords: List<Chord>,
    val fingerings: List<Fingering>,
    val score: ScoringInformation
)

private data class VoiceLeadingState(
    val score: ScoringInformation,
    val fingerings: List<Int>
)
fun bestVoiceLeadings(
    chords: List<Chord>,
    tuning: Tuning,
    opts: ScoringOptions,
    searchSpace: Int = 512
): List<VoiceLeading> {
    val fingeringsForChords = chords.map {
        getChordFingerings(it, tuning, opts, searchSpace).reversed().take(searchSpace)
    }
    val comparator = compareBy<VoiceLeadingState> { it.score.score }
    val currentStates = BoundedSolutionList(searchSpace, comparator)
    for ((idx, fingering) in fingeringsForChords.first().withIndex()) {
        currentStates.add(
                VoiceLeadingState(fingering.second, listOf(idx))
        )
    }
    fun search(idx: Int, states: List<VoiceLeadingState>): List<VoiceLeadingState> {
        if (idx == fingeringsForChords.size) {
            return states
        }
        val newStates = BoundedSolutionList(searchSpace, comparator)
        val fingerings = fingeringsForChords[idx]

        for (state in states) {
            for ((i, fingering) in fingerings.withIndex()) {
                val addedFingerings = mutableListOf<Int>()
                addedFingerings.addAll(state.fingerings)
                addedFingerings.add(i)

                val lastFingering = fingeringsForChords[idx - 1][state.fingerings.last()]
                val movementScore = scoreMovement(lastFingering.first, fingering.first, opts.movement)
                movementScore.scale(opts.movementWeight)

                movementScore.merge(fingering.second)
                movementScore.merge(state.score)

                val newState = VoiceLeadingState(movementScore, addedFingerings)
                newStates.add(newState)
            }
        }
        return search(idx + 1, newStates.toList())
    }
    val bestStates = search(1, currentStates.toList())
    return bestStates.map {
        VoiceLeading(
                chords,
                it.fingerings.mapIndexed { chordIdx, fingeringIdx ->
                    fingeringsForChords[chordIdx][fingeringIdx].first
                },
                it.score
        )
    }.sortedBy { it.score.score }
}