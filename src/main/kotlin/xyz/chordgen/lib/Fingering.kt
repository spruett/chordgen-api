package xyz.chordgen.lib

import xyz.chordgen.lib.util.BoundedSolutionList
import kotlin.math.max
import kotlin.math.roundToInt

data class FingeringScoringOptions(
    // max spacings between pairs of fingers, anything larger
    // and the chord is considered unplayable. distances are
    // in "canonical" fret lengths, where fret 0 to 1 is considered 1.0
    val maxSpacings: Map<Pair<Int, Int>, Double> = mapOf(
        (1 to 2) to 2.5,
        (1 to 3) to 3.0,
        (1 to 4) to 3.3,
        (2 to 3) to 2.0,
        (2 to 4) to 2.0,
        (3 to 4) to 2.0
    ),
    // comfortable spacing scores, if all pairs are below this
    // threshold, add comfortableSpacingScore
    val comfortableSpacings: Map<Pair<Int, Int>, Double> = mapOf(
        (1 to 2) to 1.3,
        (1 to 3) to 2.0,
        (1 to 4) to 3.0,
        (2 to 3) to 1.0,
        (2 to 4) to 1.6,
        (3 to 4) to 0.8
    ),
    val comfortableSpacingScore: Int = 18,
    // somewhat comfortable, but potential stretching, adds somewhatComfortableSpacingScore
    // if all pairs are below threshold
    val somewhatComfortableSpacing: Map<Pair<Int, Int>, Double> = mapOf(
        (1 to 2) to 2.0,
        (1 to 3) to 2.5,
        (1 to 4) to 3.0,
        (2 to 3) to 1.3,
        (2 to 4) to 1.6,
        (3 to 4) to 1.3
    ),
    val somewhatComfortableSpacingScore: Int = 13,
    // penalty for finger distance among strings
    val complexityPenalty: Double = -0.5,
    // penalty per barred finger
    val barPenalty: Int = 0,
    // additional penalty for barring on non-index finger
    val barNonIndexPenalty: Int = -7,
    // additional penalty for barring while some fingers are
    // on higher frets
    val barNonIndexWithHigher: Int = -15,
    val bracedBarBonus: Int = 5,
    val barWithPinkyHardPenalty: Int = -15,
    // additional penalties for barring specific strings, as they
    // may have higher tension
    val stringBarPenalties: Map<Int, Int> = mapOf(
        0 to -5,
        1 to -1
    ),
    // penalties (or bonus, if positive) for playing a note that
    // is not a root or fifth on these strings
    val stringNonRootOrFifthPenalties: Map<Int, Int> = mapOf(
        0 to -8,
        1 to -2,
        2 to -1,
        5 to 2
    ),
    // penalty for playing heavy strings with finger 4 (pinky)
    val pinkyHeavyStringPenalties: Map<Int, Int> = mapOf(
        0 to -8,
        1 to -3
    ),
    // number of strings considered high
    val highStringCount: Int = 2,
    // points for using highest N strings, per string
    val highStringBonus: Int = 5,
    // penalty for an inverted finger spread, e.g. pinky
    // on a lower numbered string than index finger, which
    // is hard to play
    val invertedFingerSpreadPenalty: Int = -3,
    // larger penalty for larger spread
    val largeInvertedFingerSpreadPenalty: Int = -10,
    // bonus per finger not used
    val pointsPerNonUsedFinger: Int = 2,
    // penalty for skipping strings
    val discontinuityPenalty: Int = -5,
    // larger penalty for skipping strings that are not
    // the lowest string
    val complexDiscontinuityPenalty: Int = -15,
    // target fret, see below
    val targetFret: Int = 5,
    // penalty (squared) per fret distance from targetFret above
    val targetFretSquarePenalty: Double = -0.0,
    // frets above this are considered not an open position
    val nonOpenPosition: Int = 3,
    // penalty for playing an open string when in a high position
    val openStringHighPositionPenalty: Int = -15,
    // Computed scale length for guitar, used when computing fingering
    // spacings such that fret 0 to fret 1 is 1.0
    val scaleLength: Double = 17.8171537451058
) {

    /*
     * Gets the maximum positive score acheivable with these options,
     * assuming penalties are not set to positive values.
     *
     * Used for heuristic pruning.
     */
    fun maxScore(): Int {
        return listOf(
            max(comfortableSpacingScore, somewhatComfortableSpacingScore),
            pointsPerNonUsedFinger * 4,
            highStringBonus * highStringCount
        ).sum()
    }
}

typealias Finger = Int

/*
 * A fingering is a way to play a specific chord on a guitar
 * in some tuning. It maps each pitch/fret in a voicing with
 * an appropriate finger, if it is feasible to actually
 * play that voicing.
 *
 * Fingerings come with heuristic scores for some definition of
 * playability, based on how far fingers are apart, and what
 * fingers are used for specific strings, pitches, etc.
 *
 * See above options for some examples.
 *
 * Note that not all fingerings are feasibly playable (e.g. 6 different frets),
 * so the scoring algorithm also filters out unplayable voicings.
 */
data class Fingering(
    val voicing: Voicing,
    val fingers: Map<FrettedNote, Finger>,
    val tuning: Tuning
) {
    /*
     * Scores the current fingering based on a set of heuristics. Returns a total
     * score (with breakdowns for each heuristic, see ScoringInformation). Heuristics
     * that affect score can be tuned with `opts`, see above.
     *
     * Returns null if the chord is not playable according to `opts`.
     */
    fun score(opts: FingeringScoringOptions): ScoringInformation? {
        val score = ScoringInformation("fingering")
        val spacing = playableSpacing(opts) ?: return null
        val bar = bars(opts) ?: return null
        score.add("spacing", spacing)
        score.add("bars", bar)
        score.add("free_fingers", nonUsedFingers(opts))
        score.add("skipped_strings", discontinuities(opts))
        score.add("target_fret", targetFretBonus(opts))
        score.add("open_strings", openStrings(opts))
        score.add("low_note_strings", lowNoteStrings(opts))
        score.add("pinky", pinkyHeavyString(opts))
        score.add("inverse_spread", invertedFingerSpread(opts))
        score.add("complexity", fingerComplexity(opts))
        score.add("high_strings", highStrings(opts))
        return score
    }

    private fun highStrings(opts: FingeringScoringOptions): Int {
        val highestStrings = tuning.strings.reversed().take(opts.highStringCount).map { it.index }
        return fingers.keys.sumBy {
            if (highestStrings.contains(it.string.index)) {
                opts.highStringBonus
            } else {
                0
            }
        }
    }

    private fun fingerComplexity(opts: FingeringScoringOptions): Int {
        val frets = fingers.keys.toList().sortedBy { it.string.index }
        var score = 0.0
        var lastFret: Int? = null
        for (fret in frets) {
            if (fret.isOpen()) {
                continue
            }

            if (lastFret != null) {
                score += opts.complexityPenalty * Math.abs(fret.fret - lastFret)
            }
            lastFret = fret.fret
        }
        return score.toInt()
    }

    private fun lowNoteStrings(opts: FingeringScoringOptions): Int {
        var score = 0
        val goodTypes = setOf(ChordNoteType.ROOT, ChordNoteType.FIFTH, ChordNoteType.BASS)
        for (note in fingers.keys) {
            val chordNote = voicing.chordNotes.find { it.note == note.note }
            if (chordNote != null && !goodTypes.contains(chordNote.type)) {
                val stringNumber = note.string.index
                score += opts.stringNonRootOrFifthPenalties[stringNumber] ?: 0
            }
        }
        return score
    }

    private fun openStrings(opts: FingeringScoringOptions): Int {
        val position = fingers.toList()
            .filter { !it.first.isOpen() }
            .minBy { it.second }
            ?.first?.fret ?: 0
        val anyOpen = fingers.keys.any { it.isOpen() }
        val openPosition = tuning.strings.map { it.frets.first }.max()!!
        if (anyOpen && (position - openPosition) >= opts.nonOpenPosition) {
            return opts.openStringHighPositionPenalty
        }
        return 0
    }

    private fun targetFretBonus(opts: FingeringScoringOptions): Int {
        val position = fingers.toList()
            .filter { !it.first.isOpen() }
            .minBy { it.second }
            ?.first?.fret ?: 0
        val diff = Math.abs(opts.targetFret - position)
        return (opts.targetFretSquarePenalty * diff * diff).roundToInt()
    }

    private fun pinkyHeavyString(opts: FingeringScoringOptions): Int {
        val pinkyString = fingers.toList().find {
            it.second == 4
        }?.first?.string ?: return 0
        val stringNumber = pinkyString.index
        return opts.pinkyHeavyStringPenalties[stringNumber] ?: 0
    }

    private fun invertedFingerSpread(opts: FingeringScoringOptions): Int {
        val minStringByFinger = fingers.toList().groupBy {
            it.second
        }.mapValues {
            it.value.map { it.first.string.index }.min()
        }
        val fingers = minStringByFinger.keys.sorted()
        val maxInverseSpread = fingers.map {
            val string = minStringByFinger[it]
            var nextString = minStringByFinger[it + 1]
            var idx = 1
            while (nextString == null && idx <= 4) {
                ++idx
                nextString = minStringByFinger[it + idx]
            }
            if (string != null && nextString != null) {
                string - nextString
            } else {
                0
            }
        }.max() ?: return 0
        if (maxInverseSpread <= 1) {
            return 0
        } else if (maxInverseSpread <= 2) {
            return opts.invertedFingerSpreadPenalty
        } else {
            return opts.largeInvertedFingerSpreadPenalty
        }
    }

    private fun discontinuities(opts: FingeringScoringOptions): Int {
        val fingerList = fingers.toList()
        val stringWithFingers = tuning.strings.map { string ->
            val finger = fingerList.find {
                it.first.string == string
            }?.second
            string to finger
        }.dropLastWhile { it.second == null }
        var fingersSeen = 0
        var score = 0
        var lastFinger: Finger? = null
        for ((_, finger) in stringWithFingers) {
            if (finger != null) {
                fingersSeen++
                lastFinger = finger
            } else if (fingersSeen > 0) {
                score += opts.discontinuityPenalty
                if (lastFinger != 1 || fingersSeen > 1) {
                    score += opts.complexDiscontinuityPenalty
                }
            }
        }
        return score
    }

    private fun nonUsedFingers(opts: FingeringScoringOptions): Int {
        val used = fingers.filter { !it.key.isOpen() }.values.distinct().size
        return (4 - used) * opts.pointsPerNonUsedFinger
    }

    private fun bars(opts: FingeringScoringOptions): Int? {
        val repeatedFingers = fingers
            .filter { !it.key.isOpen() }
            .map { it.value to it.key }
            .groupBy({ it.first }, { it.second })
            .filter { it.value.size > 1 }
        if (repeatedFingers.isEmpty()) {
            return 0
        }
        var score = opts.barPenalty * repeatedFingers.size

        val byString = fingers.filter { !it.key.isOpen() }
            .map { it.key.string.index to (it.key.fret to it.value) }
            .groupBy({ it.first }, { it.second })
            .mapValues { it.value.first() }

        for ((finger, frets) in repeatedFingers) {
            val fret = frets.first().fret
            if (finger != 1) {
                score += if (finger == 4 && frets.size > 2) {
                    opts.barWithPinkyHardPenalty
                } else {
                    opts.barNonIndexPenalty
                }

                val potentialBrace = byString[frets.map { it.string.index }.min()?.minus(1)]
                if (potentialBrace != null && potentialBrace.first == fret && potentialBrace.second < finger) {
                    score += opts.bracedBarBonus
                }

                if (fingers.keys.any { it.fret > fret }) {
                    score += opts.barNonIndexWithHigher
                }
            }
            val minString = frets.map { it.string.index }.min()!!
            val maxString = frets.map { it.string.index }.max()!!
            val barInterrupted = fingers.keys.any {
                it.string.index > minString && it.fret < fret
            }
            if (barInterrupted) {
                return null
            }
            for (string in minString..maxString) {
                score += opts.stringBarPenalties[string] ?: 0
            }
        }

        return score
    }

    private fun playableSpacing(opts: FingeringScoringOptions): Int? {
        val inverted = fingers.map { it.value to it.key.fret }.toMap()
        var comfortable = true
        var somewhatComfortable = true
        for (i in 1..4) {
            for (j in (i + 1)..4) {
                val lhs = inverted[i]
                val rhs = inverted[j]
                val maxSpacing = opts.maxSpacings[i to j]!!
                if (lhs != null && rhs != null) {
                    val dist = fretDistance(lhs, rhs, opts)
                    if (dist > maxSpacing) {
                        return null
                    }
                    if (dist > opts.comfortableSpacings[i to j]!!) {
                        comfortable = false
                    }
                    if (dist > opts.somewhatComfortableSpacing[i to j]!!) {
                        somewhatComfortable = false
                    }
                }
            }
        }
        if (comfortable) {
            return opts.comfortableSpacingScore
        } else if (somewhatComfortable) {
            return opts.somewhatComfortableSpacingScore
        }
        return 0
    }
}

/*
 * Gets all fingerings that are playable for a given voicing (on a
 * given tuning) scored by `opts`.
 *
 * Returned list is sorted by score descending.
 */
fun getFingerings(
    voicing: Voicing,
    tuning: Tuning,
    opts: FingeringScoringOptions
): List<Pair<Fingering, ScoringInformation>> {
    val fretted = voicing.notes
        .sortedWith(compareBy<FrettedNote> { it.fret }.thenBy { it.string.index })

    val fingerings = mutableListOf<Fingering>()
    val current = mutableListOf<Pair<FrettedNote, Finger>>()
    fun search(idx: Int, finger: Finger, lastFret: Int? = null) {
        if (idx == fretted.size) {
            fingerings.add(Fingering(voicing, current.toMap(), tuning))
            return
        }
        if (finger > 4) {
            return
        }

        val note = fretted[idx]
        if (note.isOpen()) {
            current.add(note to 0)
            search(idx + 1, finger, note.fret)
            current.removeAt(current.size - 1)
        } else {
            // try a bar
            if (note.fret == lastFret) {
                current.add(note to finger)
                search(idx + 1, finger, note.fret)
                current.removeAt(current.size - 1)
            }
            // no bar
            for (i in 1..3) {
                if (finger + i <= 4) {
                    current.add(note to finger + i)
                    search(idx + 1, finger + i, note.fret)
                    current.removeAt(current.size - 1)
                }
            }
        }
    }
    search(0, 0)

    val scored = fingerings.mapNotNull {
        val score = it.score(opts)
        if (score != null) {
            it to score
        } else null
    }
    return scored.sortedByDescending { it.second.score }
}

/*
 * Gets the top `maxResults` fingerings for a given chord across all voicings,
 * as the top fingering for each voicing, along with individual scoring information.
 *
 * Voicings (mapping of notes to frets) are first generated and scored according
 * to `opts`. Then individual fingerings (which fingers to use on each fret) are generated
 * and scored.
 *
 * All returned fingerings are playable (by the heuristic definition given in Fingering, not
 * the weak heuristic in Voicing).
 *
 * Returned list is sorted by score descending.
 */
fun getChordFingerings(
    chord: Chord,
    tuning: Tuning,
    opts: ScoringOptions,
    maxResults: Int = 512
): List<Pair<Fingering, ScoringInformation>> {
    val voicings = findVoicings(chord, tuning)
    val resultSet = BoundedSolutionList(
        maxResults,
        compareBy<Pair<Fingering, ScoringInformation>> { it.second.score }
    )
    fun compactResults(): Boolean {
        resultSet.compact {
            takeNonSimilarVoicings(maxResults, it, { it.first.voicing }, opts.dedup)
        }
        return !resultSet.isFull()
    }
    val scoredVoicings = voicings
        .map {
            val voicingScore = scoreVoicing(it, opts.voicing)
            voicingScore.scale(opts.voicingWeight)
            it to voicingScore
        }
        .sortedByDescending { it.second.score }

    for ((voicing, score) in scoredVoicings) {
        if (resultSet.isFull() && resultSet.peek().second.score >= score.score + opts.fingering.maxScore()) {
            if (!compactResults()) {
                break
            }
        }
        val bestFingering = getFingerings(voicing, tuning, opts.fingering).firstOrNull() ?: continue
        bestFingering.second.scale(opts.fingeringWeight)
        score.merge(bestFingering.second)

        val scored = bestFingering.first to score
        resultSet.add(scored)
    }
    compactResults()
    return resultSet.toList()
}
