package xyz.chordgen.lib

data class Note(val name: NoteName, val flats: Int = 0, val sharps: Int = 0) {
    init {
        check(flats == 0 || sharps == 0)
    }

    override fun toString(): String {
        val sharpSigns = "♯".repeat(sharps)
        val flatSigns = "♭".repeat(flats)
        return "$name$sharpSigns$flatSigns"
    }

    fun toSemitoneOffset(): Int {
        return normalizeSemitoneOffset(name.toSemitoneOffset() + sharps - flats)
    }

    fun addInterval(interval: Interval): Note {
        val newName = name.add(interval.range - 1)
        val offset = interval.toSemitoneOffset()
        return fromSemitoneOffset(toSemitoneOffset() + offset, newName)
    }

    fun intervalFrom(other: Note): Interval {
        val interval = name.difference(other.name) + 1
        val semitones = normalizeSemitoneOffset(toSemitoneOffset() - other.toSemitoneOffset())
        return Interval.fromRangeAndSemitoneOffset(interval, semitones)
    }

    companion object {
        fun fromString(name: String): Note {
            val noteName = NoteName.valueOf(name.substring(0, 1))

            val modifiers = name.substring(1)
            var flats = 0
            var sharps = 0
            for (modifier in modifiers) {
                when (modifier) {
                    '#' -> sharps++
                    '♯' -> sharps++
                    'b' -> flats++
                    '♭' -> flats++
                    else -> throw UserException("Invalid note modifier: $modifier in note $name")
                }
            }
            if (flats != 0 && sharps != 0) {
                throw UserException("Invalid note $name, got both sharps and flats")
            }
            return Note(noteName, flats, sharps)
        }

        fun fromSemitoneOffset(offset: Int): Note {
            val offsetInRange = normalizeSemitoneOffset(offset)
            val notes = arrayOf(
                    fromString("C"),
                    fromString("Db"),
                    fromString("D"),
                    fromString("Eb"),
                    fromString("E"),
                    fromString("F"),
                    fromString("Gb"),
                    fromString("G"),
                    fromString("Ab"),
                    fromString("A"),
                    fromString("Bb"),
                    fromString("B")
            )
            return notes[offsetInRange]
        }

        fun fromSemitoneOffset(offset: Int, nameHint: NoteName): Note {
            for (i in 0..12) {
                val withFlats = Note(nameHint, flats = i)
                val withSharps = Note(nameHint, sharps = i)
                if (withFlats.toSemitoneOffset() == normalizeSemitoneOffset(offset)) {
                    return withFlats
                } else if (withSharps.toSemitoneOffset() == normalizeSemitoneOffset(offset)) {
                    return withSharps
                }
            }
            throw IllegalStateException("Invalid offset $offset for name $nameHint")
        }
    }
}