package xyz.chordgen.lib

import xyz.chordgen.proto.ErrorCode

/*
 * Generic exception used when user inputs something garbage. Used for
 * error reporting in UIs.
 */
class UserException(
    override val message: String,
    val code: ErrorCode = ErrorCode.NONE
) : Exception()
