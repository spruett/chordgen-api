package xyz.chordgen.lib

enum class NoteName(val note: Char) {
    A('A'),
    B('B'),
    C('C'),
    D('D'),
    E('E'),
    F('F'),
    G('G');

    fun toSemitoneOffset(): Int {
        return when (this) {
            C -> 0
            D -> 2
            E -> 4
            F -> 5
            G -> 7
            A -> 9
            B -> 11
        }
    }

    fun add(offset: Int): NoteName {
        val next = normalizeInterval(note - 'A' + offset) + 'A'.toInt()
        return fromChar(next.toChar())
    }

    fun difference(name: NoteName): Int {
        val difference = (note - 'A') - (name.note - 'A')
        return normalizeInterval(difference)
    }

    companion object {
        val index = NoteName.values().associateBy { it.note }
        fun fromChar(note: Char): NoteName {
            return index[note]!!
        }
    }
}