package xyz.chordgen.lib

import kotlin.math.max
import kotlin.math.min

data class FrettedNote(
    val note: Note,
    val string: GuitarString,
    val pitch: Pitch
) {
    val fret = pitch.semitone - string.openPitch.semitone

    fun isOpen(): Boolean {
        return fret == string.frets.first
    }
}

val MAX_FRET_SPACING = 5.0

data class Voicing(val chord: Chord, val notes: List<FrettedNote>) {
    init {
        check(notes.size >= 2)
    }

    val chordNotes: List<ChordNote> by lazy {
        val fretNotes = notes.sortedBy { it.pitch.semitone }.map { it.note }
        fretNotes.mapNotNull { note ->
            chord.notes.find { it.note == note }
        }
    }

    fun playable(): Boolean {
        // basic playability checks, no scoring
        val nonOpenFrets = notes.filter { !it.isOpen() }.map { it.fret }.sorted()
        val distinctNotes = notes.map { it.pitch.semitoneOffset() }.distinct()
        if (distinctNotes.size < 2) {
            return false
        }

        if (nonOpenFrets.distinct().size > 4) {
            // not enough fingers
            return false
        }
        if (nonOpenFrets.size >= 2) {
            if (fretDistance(nonOpenFrets.last(), nonOpenFrets.first()) > MAX_FRET_SPACING) {
                return false
            }
        }

        // prune out chords that don't match requirements
        val bass = chord.getBass()
        if (bass != null) {
            val lowest = notes.minBy { it.pitch.semitone }
            if (lowest?.note != bass.note) {
                return false
            }
        }
        val melody = chord.getMelody()
        if (melody != null) {
            val highest = notes.maxBy { it.pitch.semitone }
            if (highest?.note != melody.note) {
                return false
            }
        }
        return true
    }

    companion object {
        fun fromFrets(frets: List<Int?>, chord: Chord, tuning: Tuning): Voicing {
            check(frets.size == tuning.strings.size)

            val fretted = frets.mapIndexedNotNull { idx: Int, fret: Int? ->
                if (fret != null) {
                    val string = tuning.strings[idx]
                    val pitch = Pitch(string.openPitch.semitone + fret)
                    val note = chord.noteFromPitch(pitch)?.note
                        ?: throw UserException("No note found in $chord that matches given pitch on string ${string.name}")
                    FrettedNote(note, string, pitch)
                } else {
                    null
                }
            }
            return Voicing(chord, fretted)
        }
    }
}

val MIN_NOTES_FOR_CHORD = 3
fun findVoicings(chord: Chord, tuning: Tuning): List<Voicing> {
    val voicings = mutableListOf<Voicing>()
    val semitonesToNotes = chord.notes.map {
        it.note.toSemitoneOffset() to it.note
    }.toMap()

    fun fretWithinRange(fret: Int, range: Pair<Int, Int>?): Boolean {
        if (range == null) {
            return true
        }
        return fret - range.first <= MAX_FRET_SPACING &&
            range.second - fret <= MAX_FRET_SPACING
    }
    fun mergeFretRange(fret: Int, range: Pair<Int, Int>?): Pair<Int, Int> {
        if (range == null) {
            return Pair(fret, fret)
        }
        return Pair(min(fret, range.first), max(fret, range.second))
    }

    fun search(idx: Int, voices: MutableList<FrettedNote>, fretRange: Pair<Int, Int>? = null) {
        if (idx == tuning.strings.size) {
            return
        }

        val string = tuning.strings[idx]

        for (fret in string.frets) {
            if (fret != string.frets.start && !fretWithinRange(fret, fretRange)) {
                continue
            }
            val pitch = string.fretPitch(fret)
            val note = semitonesToNotes[pitch.semitoneOffset()]
            if (note != null) {
                voices.add(FrettedNote(note, string, pitch))
                if (voices.size >= MIN_NOTES_FOR_CHORD) {
                    val voicing = Voicing(chord, voices.toList())
                    if (voicing.playable()) {
                        voicings.add(voicing)
                    }
                }
                search(idx + 1, voices, mergeFretRange(fret, fretRange))
                voices.removeAt(voices.size - 1)
            }
        }
        search(idx + 1, voices, fretRange)
    }
    search(0, mutableListOf())
    return voicings
}