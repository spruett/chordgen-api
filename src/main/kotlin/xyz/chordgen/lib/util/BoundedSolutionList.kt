package xyz.chordgen.lib.util

import java.util.PriorityQueue

/*
 * A bounded, sorted heap structure that maintains
 * the top-K elements. Implemented using a binary heap.
 */
class BoundedSolutionList<T>(
    private val bound: Int,
    private val comparator: Comparator<T>
) : Iterable<T> {

    private val heap = PriorityQueue<T>(comparator)

    /*
     * Adds value if set is not yet full or if value is
     * greater than the minimum currently tracked element.
     */
    fun add(value: T) {
        if (heap.size >= bound) {
            val lowest = peek()
            if (comparator.compare(value, lowest) <= 0) {
                return
            }
            heap.poll()
        }
        heap.add(value)
    }

    fun compact(compactFn: (Iterable<T>) -> Iterable<T>) {
        val newHeap = compactFn(heap.toList().sortedWith(comparator).reversed())
        heap.clear()
        heap.addAll(newHeap)
    }

    /*
     * Gives the lowest value currently stored.
     */
    fun peek(): T {
        return heap.first()
    }

    /*
     * Returns true if the set is full (currently at its bound).
     */
    fun isFull(): Boolean {
        return heap.size >= bound
    }

    /*
     * Maps a function (as in List::map(f)) over the current solution set.
     * Elements are not returned in a particular order.
     */
    fun <V> map(fn: (T) -> V): List<V> {
        return heap.map { fn(it) }
    }

    /*
     * Clears the set of tracked items and returns them in sorted order,
     * lowest first.
     */
    fun toList(): List<T> {
        return heap.toList().sortedWith(comparator)
    }

    override fun iterator(): Iterator<T> {
        return heap.iterator()
    }
}
