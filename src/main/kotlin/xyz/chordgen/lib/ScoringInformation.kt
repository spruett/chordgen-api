package xyz.chordgen.lib

data class ScoringInformation(
    val prefix: String,
    var score: Int = 0,
    val subclasses: MutableMap<String, Int> = mutableMapOf()
) {
    fun add(category: String, score: Int) {
        this.score += score
        subclasses["$prefix.$category"] = score
    }
    fun merge(other: ScoringInformation) {
        score += other.score
        subclasses.putAll(other.subclasses)
    }
    fun scale(scale: Int) {
        score *= scale
        for ((subclass, score) in subclasses) {
            subclasses[subclass] = score * scale
        }
    }

    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("score = $score\n")
        for ((subclass, score) in subclasses) {
            builder.append("$subclass = $score\n")
        }
        return builder.toString()
    }
}