package xyz.chordgen.lib

import xyz.chordgen.proto.NoteList
import xyz.chordgen.proto.NoteType
import xyz.chordgen.proto.PlayedNote
import xyz.chordgen.proto.VoicingSet

/*
 * General utilities from converting the library datatypes (used for computation)
 * to the protobuf forms which can be serialized. All take the form translateT(T) -> proto.T
 *
 * Note that not all fields are useful for clients, so some information is discarded.
 */

fun translateInterval(interval: Interval): xyz.chordgen.proto.Interval {
    return xyz.chordgen.proto.Interval.newBuilder()
        .setRange(interval.range)
        .setFlats(interval.flats)
        .setSharps(interval.sharps)
        .build()
}

fun translateNote(note: Note, type: ChordNoteType? = null, interval: Interval? = null): xyz.chordgen.proto.Note {
    val builder = xyz.chordgen.proto.Note.newBuilder()
        .setName(note.toString())
    if (type != null) {
        val protoType = when (type) {
            ChordNoteType.BASS -> NoteType.BASS
            ChordNoteType.ROOT -> NoteType.ROOT
            ChordNoteType.THIRD -> NoteType.THIRD
            ChordNoteType.FIFTH -> NoteType.FIFTH
            ChordNoteType.SEVENTH -> NoteType.SEVENTH
            ChordNoteType.EXTENSION -> NoteType.EXTENSION
            ChordNoteType.MELODY -> NoteType.MELODY
        }
        builder.type = protoType
    }
    if (interval != null) {
        builder.interval = translateInterval(interval)
    }
    return builder.build()
}

fun translateString(string: GuitarString): xyz.chordgen.proto.GuitarString {
    return xyz.chordgen.proto.GuitarString.newBuilder()
        .setName(string.name)
        .setIndex(string.index)
        .build()
}

fun translateChordFlavor(chordFlavor: ChordFlavor): xyz.chordgen.proto.ChordFlavor {
    return when (chordFlavor) {
        ChordFlavor.MAJOR -> xyz.chordgen.proto.ChordFlavor.MAJOR
        ChordFlavor.MINOR -> xyz.chordgen.proto.ChordFlavor.MINOR
        ChordFlavor.AUGMENTED -> xyz.chordgen.proto.ChordFlavor.AUGMENTED
        ChordFlavor.DIMINISHED -> xyz.chordgen.proto.ChordFlavor.DIMINISHED
    }
}

fun translateChord(chord: Chord): xyz.chordgen.proto.Chord {
    val flavors = mutableListOf<xyz.chordgen.proto.ChordFlavor>()
    flavors.add(translateChordFlavor(chord.getFlavor()))
    fun addFlavor(flavor: xyz.chordgen.proto.ChordFlavor) {
        if (!flavors.contains(flavor)) {
            flavors.add(flavor)
        }
    }

    val inTriad = setOf(chord.getRoot().note, chord.getThird().note, chord.getFifth().note)
    for (note in chord.getExtensions()) {
        val interval = note.note.intervalFrom(chord.getRoot().note)
        if (interval.range == 7 && interval.flats == 1) {
            addFlavor(xyz.chordgen.proto.ChordFlavor.DOMINANT)
        } else if (interval.range == 7 && interval.flats == 0 && interval.sharps == 0) {
            addFlavor(xyz.chordgen.proto.ChordFlavor.MAJOR_SEVEN)
        } else if (interval.range == 6 && interval.flats == 0 && interval.sharps == 0) {
            addFlavor(xyz.chordgen.proto.ChordFlavor.SIXTH)
        } else if (!inTriad.contains(note.note)) {
            addFlavor(xyz.chordgen.proto.ChordFlavor.EXTENSIONS)
        }
    }

    return xyz.chordgen.proto.Chord.newBuilder()
        .setName(chord.toString())
        .addAllFlavors(flavors)
        .addAllNotes(chord.notes.map {
            translateNote(it.note, it.type, it.note.intervalFrom(chord.getRoot().note))
        })
        .build()
}

fun translateTuning(tuning: Tuning): xyz.chordgen.proto.Tuning {
    // For now, everything is standard tuning
    return xyz.chordgen.proto.Tuning.newBuilder()
        .setId(tuning.id)
        .setCapo(tuning.strings.first().frets.first)
        .addAllStrings(tuning.strings.map {
            translateString(it)
        })
        .build()
}

fun translateFingerings(chord: Chord, tuning: Tuning, fingerings: Iterable<Fingering>): VoicingSet {
    return VoicingSet.newBuilder()
        .setChord(translateChord(chord))
        .setTuning(translateTuning(tuning))
        .addAllVoicings(fingerings.map {
            NoteList.newBuilder()
                .addAllNotes(it.fingers.entries.map { (note, finger) ->
                    PlayedNote.newBuilder()
                        .setNote(translateNote(note.note))
                        .setFret(note.fret)
                        .setFinger(finger)
                        .setPitch(note.pitch.frequency())
                        .setString(translateString(note.string))
                        .build()
                })
                .build()
        })
        .build()
}
