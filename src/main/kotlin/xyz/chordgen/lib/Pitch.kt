package xyz.chordgen.lib

data class Pitch(val semitone: Int) {
    fun octave(): Int {
        return semitone / 12
    }

    fun semitoneOffset(): Int {
        return semitone % 12
    }

    fun frequency(): Double {
        val a4 = fromNoteAndOctave(Note.fromString("A"), 4)
        return 440.0 * Math.pow(2.0, (semitone - a4.semitone) / 12.0)
    }

    override fun toString(): String {
        val note = Note.fromSemitoneOffset(semitoneOffset())
        return "Pitch(${note}_${octave()})"
    }

    companion object {
        fun fromNoteAndOctave(note: Note, octave: Int): Pitch {
            return Pitch(note.toSemitoneOffset() + 12 * octave)
        }
    }
}