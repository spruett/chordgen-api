package xyz.chordgen.lib

/*
 * Computes the edit distance (using the typical dynamic programming algorithm)
 * between two voicings, specified as a set of FrettedNotes.
 *
 * Edit distance is specified as the minimum number of edits (add one, remove one,
 * or change one is an edit) to make `lhs` into `rhs`.
 *
 * This distance is a heuristic for eliminating voicings that are very similar (e.g.
 * only one note added or changed).
 */
private fun voicingDistance(lhs: List<FrettedNote>, rhs: List<FrettedNote>): Int {
    val left = lhs.sortedBy { it.string.openPitch.semitone }
    val right = rhs.sortedBy { it.string.openPitch.semitone }

    val cache = mutableMapOf<Pair<Int, Int>, Int>()
    fun editDistance(i: Int, j: Int): Int {
        val cached = cache[i to j]
        if (cached != null) {
            return cached
        }
        if (i == left.size) {
            return right.size - j
        }
        if (j == right.size) {
            return left.size - i
        }

        val ans = if (left[i] == right[j]) {
            editDistance(i + 1, j + 1)
        } else {
            1 + listOf(
                editDistance(i + 1, j),
                editDistance(i, j + 1),
                editDistance(i + 1, j + 1)
            ).min()!!
        }
        cache[i to j] = ans
        return ans
    }
    return editDistance(0, 0)
}

/*
 * Returns true if `lhs` and `rhs` are past the similarity threshold in
 * opts, based on computing their edit distance.
 */
private fun isSimilar(lhs: Voicing, rhs: Voicing, opts: DedupOptions): Boolean {
    return voicingDistance(lhs.notes, rhs.notes) <= opts.voicingNoteDiffThreshold
}

private fun isSubset(superset: Voicing, subset: Voicing): Boolean {
    return superset.notes.toSet().containsAll(subset.notes)
}

/*
 * Returns true if `lhs` and `rhs` contain enough similar voicings (see isSimilar(Voicing, Voicing, DedupOptions)
 * to pass the threshold percentage in opts.
 */
private fun isSimilarLeading(lhs: VoiceLeading, rhs: VoiceLeading, opts: DedupOptions): Boolean {
    val similarChords = lhs.fingerings.zip(rhs.fingerings).count { (left, right) ->
        isSimilar(left.voicing, right.voicing, opts)
    }
    return similarChords.toDouble() / lhs.fingerings.size.toDouble() >= opts.leadingChordDiffRatio
}

data class DedupOptions(
    // More than this many notes must be different in a voicing for them
    // to not be considered "similar" and deduped
    val voicingNoteDiffThreshold: Int = 1,
    // Drop total voicing subsets if the superset is at least this many indices
    // above in the list, removes much lower scoring subsets
    val voicingDropSubSetIndexDifference: Int = 2,
    // More than this fraction of chords in a voice leading must be not similar
    // (as defined above) for the leadings to not be deduped
    val leadingChordDiffRatio: Double = 0.6
)

/*
 * Takes up to `n` "non-similar" voicings from a given iterable (voicing extracted via
 * `mapper`), as defined by isSimilar() and `opts`.
 *
 */
fun <T> takeNonSimilarVoicings(
    n: Int,
    elems: Iterable<T>,
    mapper: (T) -> Voicing,
    opts: DedupOptions
): List<T> {
    val nonSimilar = mutableListOf<T>()
    for (item in elems) {
        if (nonSimilar.size >= n) {
            break
        }

        val mapped = mapper(item)
        val similar = nonSimilar.any { isSimilar(mapper(it), mapped, opts) }
        if (similar) {
            continue
        }
        val anySubset = nonSimilar.dropLast(opts.voicingDropSubSetIndexDifference)
            .any { isSubset(mapper(it), mapped) }
        if (!anySubset) {
            nonSimilar.add(item)
        }
    }
    return nonSimilar
}

/*
 * Takes up to `n` "non-similar" leaadings as defined by isSimilarLeading and `opts`.
 * Each leading returned is not similar to any other in the returned list.
 */
fun takeNonSimilarLeadings(n: Int, list: List<VoiceLeading>, opts: DedupOptions): List<VoiceLeading> {
    val nonSimilar = mutableListOf<VoiceLeading>()
    for (item in list) {
        if (nonSimilar.size >= n) {
            break
        }

        val similar = nonSimilar.any { isSimilarLeading(it, item, opts) }
        if (!similar) {
            nonSimilar.add(item)
        }
    }
    return nonSimilar
}
