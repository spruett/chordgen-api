package xyz.chordgen.lib

private fun fretPlacement(fret: Int, opts: FingeringScoringOptions): Double {
    // pick a scale length such that fret 1 is 1 unit distance
    val scaleLength = opts.scaleLength
    return scaleLength - (scaleLength / (Math.pow(2.0, fret.toDouble() / 12)))
}

/*
 * Gets the canonical fret distance between two frets (as an absolute value), tuned
 * by opts.scaleLength such that 1.0 is the distance to fret 1 on a standard guitar.
 *
 * Scoring heuristics are tuned to this fret distance (based on my hand, which
 * may or may not be average).
 */
fun fretDistance(left: Int, right: Int, opts: FingeringScoringOptions = FingeringScoringOptions()): Double {
    return Math.abs(fretPlacement(left, opts) - fretPlacement(right, opts))
}