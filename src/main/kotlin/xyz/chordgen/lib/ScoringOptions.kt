package xyz.chordgen.lib

import xyz.chordgen.proto.Settings
import xyz.chordgen.proto.TuningId

data class ScoringOptions(
    val voicing: VoicingScoringOptions = VoicingScoringOptions(),
    val fingering: FingeringScoringOptions = FingeringScoringOptions(),
    val movement: MovementScoringOptions = MovementScoringOptions(),
    val dedup: DedupOptions = DedupOptions(),
    val voicingWeight: Int = 3,
    val fingeringWeight: Int = 3,
    val movementWeight: Int = 1
)

fun settingsToOptions(settings: Settings): ScoringOptions {
    var baseOptions = ScoringOptions()

    if (settings.disableBars) {
        baseOptions = baseOptions.copy(
            fingering = baseOptions.fingering.copy(
                barPenalty = -10000
            )
        )
    }
    if (settings.disableDedup || settings.tuning == TuningId.UKULELE) {
        baseOptions = baseOptions.copy(
            dedup = baseOptions.dedup.copy(
                voicingNoteDiffThreshold = 0,
                voicingDropSubSetIndexDifference = Int.MAX_VALUE
            )
        )
    }
    if (settings.targetFret > 0) {
        baseOptions = baseOptions.copy(
            fingering = baseOptions.fingering.copy(
                targetFret = settings.targetFret,
                targetFretSquarePenalty = -5.0
            )
        )
    } else if (settings.tuning == TuningId.UKULELE) {
        baseOptions = baseOptions.copy(
            fingering = baseOptions.fingering.copy(
                targetFret = 1,
                targetFretSquarePenalty = -1.0
            )
        )
    }

    if (settings.tuning == TuningId.UKULELE) {
        baseOptions = baseOptions.copy(
            voicing = baseOptions.voicing.copy(
                repeatedPitchPenalty = 0
            )
        )
    }
    return baseOptions
}