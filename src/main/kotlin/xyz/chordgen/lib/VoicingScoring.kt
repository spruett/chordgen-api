package xyz.chordgen.lib

private fun totalNotes(voicing: Voicing, opts: VoicingScoringOptions): Int {
    var score = voicing.notes.size * opts.perNoteScore
    if (voicing.notes.size >= 4) {
        score += opts.pointsForFourNotes
    }
    return score
}

private fun distinctNotes(voicing: Voicing, opts: VoicingScoringOptions): Int {
    return voicing.notes.map { it.note }.distinct().size * opts.distinctNoteScore
}

private fun bassNote(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val bass = voicing.chordNotes.first()
    if (bass.type == ChordNoteType.ROOT) {
        return opts.rootOnBottom
    }
    if (bass.type == ChordNoteType.FIFTH) {
        return opts.fifthOnBottom
    }
    if (voicing.chord.getRoot().note.addInterval(Interval(7)) == bass.note) {
        return opts.majorSeventhOnBottom
    }
    return 0
}

private fun explicitNotes(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val explicits = setOf(ChordNoteType.EXTENSION, ChordNoteType.SEVENTH)
    return voicing.chordNotes.distinct()
            .filter { it.explicit && explicits.contains(it.type) }
            .sumBy { opts.includesExplicit }
}

private fun doubledExtensions(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val hist = voicing.chordNotes
            .groupBy { it.type }.mapValues { it.value.size - 1 }
            .toMutableMap()
    val extTypes = listOf(
            ChordNoteType.EXTENSION,
            ChordNoteType.SEVENTH,
            ChordNoteType.BASS,
            ChordNoteType.MELODY
    )
    fun removeTypeIfNormalNote(note: Note, type: ChordNoteType) {
        val isNormalNote = voicing.chord.notes.any {
            it.note.toSemitoneOffset() == note.toSemitoneOffset() && !extTypes.contains(it.type)
        }
        if (isNormalNote) {
            hist.remove(type)
        }
    }

    voicing.chord.getBass()?.note?.let {
        removeTypeIfNormalNote(it, ChordNoteType.BASS)
    }
    voicing.chord.getMelody()?.note?.let {
        removeTypeIfNormalNote(it, ChordNoteType.MELODY)
    }
    return extTypes.sumBy {
        hist.getOrDefault(it, 0) * opts.doubleExtensionPenalty
    }
}

private fun repeatedPitchPenalty(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val repeated = voicing.notes.groupBy { it.pitch.semitone }.filter { it.value.size > 1 }
    return repeated.size * opts.repeatedPitchPenalty
}

private fun thirdBonus(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val hasThird = voicing.notes.any {
        it.note == voicing.chord.getThird().note
    }
    return if (hasThird) {
        opts.thirdBonus
    } else {
        0
    }
}

private fun fifthWithSeventh(voicing: Voicing, opts: VoicingScoringOptions): Int {
    val hasSeventhOrSixth = voicing.notes.any {
        val interval = it.note.intervalFrom(voicing.chord.getRoot().note).range
        interval == 6 || interval == 7
    }
    if (hasSeventhOrSixth) {
        val hasFifth = voicing.notes.any {
            it.note == voicing.chord.getFifth().note
        }
        if (hasFifth) {
            return opts.fifthWithSeventhPenalty
        }
    }
    return 0
}

data class VoicingScoringOptions(
    // more points for more notes
    val perNoteScore: Int = 3,
    val pointsForFourNotes: Int = 20,
    // points per distinct note
    val distinctNoteScore: Int = 12,
    // bonus for root on bottom
    val rootOnBottom: Int = 15,
    val fifthOnBottom: Int = 7,
    val majorSeventhOnBottom: Int = -10,
    // includes explicitly listed intervals (e.g. 9 or 11s)
    val includesExplicit: Int = 15,
    val repeatedPitchPenalty: Int = -50,
    // don't double 7s or extensions
    val doubleExtensionPenalty: Int = -10,
    // bonus for including the third
    val thirdBonus: Int = 5,
    val fifthWithSeventhPenalty: Int = -10
)

fun scoreVoicing(voicing: Voicing, opts: VoicingScoringOptions): ScoringInformation {
    val score = ScoringInformation("voicing")
    score.add("distinct_notes", distinctNotes(voicing, opts))
    score.add("explicit_notes", explicitNotes(voicing, opts))
    score.add("doubled_extensions", doubledExtensions(voicing, opts))
    score.add("total_notes", totalNotes(voicing, opts))
    score.add("bass", bassNote(voicing, opts))
    score.add("third", thirdBonus(voicing, opts))
    score.add("5_with_7", fifthWithSeventh(voicing, opts))
    score.add("repeated_pitches", repeatedPitchPenalty(voicing, opts))
    return score
}
