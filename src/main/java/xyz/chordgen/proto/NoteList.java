// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

/**
 * <pre>
 * Composite types for actual messages
 * </pre>
 *
 * Protobuf type {@code chordgen.NoteList}
 */
public  final class NoteList extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:chordgen.NoteList)
    NoteListOrBuilder {
  // Use NoteList.newBuilder() to construct.
  private NoteList(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private NoteList() {
    notes_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private NoteList(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              notes_ = new java.util.ArrayList<xyz.chordgen.proto.PlayedNote>();
              mutable_bitField0_ |= 0x00000001;
            }
            notes_.add(
                input.readMessage(xyz.chordgen.proto.PlayedNote.parser(), extensionRegistry));
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
        notes_ = java.util.Collections.unmodifiableList(notes_);
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_NoteList_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_NoteList_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            xyz.chordgen.proto.NoteList.class, xyz.chordgen.proto.NoteList.Builder.class);
  }

  public static final int NOTES_FIELD_NUMBER = 1;
  private java.util.List<xyz.chordgen.proto.PlayedNote> notes_;
  /**
   * <code>repeated .chordgen.PlayedNote notes = 1;</code>
   */
  public java.util.List<xyz.chordgen.proto.PlayedNote> getNotesList() {
    return notes_;
  }
  /**
   * <code>repeated .chordgen.PlayedNote notes = 1;</code>
   */
  public java.util.List<? extends xyz.chordgen.proto.PlayedNoteOrBuilder> 
      getNotesOrBuilderList() {
    return notes_;
  }
  /**
   * <code>repeated .chordgen.PlayedNote notes = 1;</code>
   */
  public int getNotesCount() {
    return notes_.size();
  }
  /**
   * <code>repeated .chordgen.PlayedNote notes = 1;</code>
   */
  public xyz.chordgen.proto.PlayedNote getNotes(int index) {
    return notes_.get(index);
  }
  /**
   * <code>repeated .chordgen.PlayedNote notes = 1;</code>
   */
  public xyz.chordgen.proto.PlayedNoteOrBuilder getNotesOrBuilder(
      int index) {
    return notes_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < notes_.size(); i++) {
      output.writeMessage(1, notes_.get(i));
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < notes_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, notes_.get(i));
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof xyz.chordgen.proto.NoteList)) {
      return super.equals(obj);
    }
    xyz.chordgen.proto.NoteList other = (xyz.chordgen.proto.NoteList) obj;

    boolean result = true;
    result = result && getNotesList()
        .equals(other.getNotesList());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptorForType().hashCode();
    if (getNotesCount() > 0) {
      hash = (37 * hash) + NOTES_FIELD_NUMBER;
      hash = (53 * hash) + getNotesList().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static xyz.chordgen.proto.NoteList parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.NoteList parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.NoteList parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.NoteList parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(xyz.chordgen.proto.NoteList prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * <pre>
   * Composite types for actual messages
   * </pre>
   *
   * Protobuf type {@code chordgen.NoteList}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:chordgen.NoteList)
      xyz.chordgen.proto.NoteListOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_NoteList_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_NoteList_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              xyz.chordgen.proto.NoteList.class, xyz.chordgen.proto.NoteList.Builder.class);
    }

    // Construct using xyz.chordgen.proto.NoteList.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getNotesFieldBuilder();
      }
    }
    public Builder clear() {
      super.clear();
      if (notesBuilder_ == null) {
        notes_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        notesBuilder_.clear();
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_NoteList_descriptor;
    }

    public xyz.chordgen.proto.NoteList getDefaultInstanceForType() {
      return xyz.chordgen.proto.NoteList.getDefaultInstance();
    }

    public xyz.chordgen.proto.NoteList build() {
      xyz.chordgen.proto.NoteList result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public xyz.chordgen.proto.NoteList buildPartial() {
      xyz.chordgen.proto.NoteList result = new xyz.chordgen.proto.NoteList(this);
      int from_bitField0_ = bitField0_;
      if (notesBuilder_ == null) {
        if (((bitField0_ & 0x00000001) == 0x00000001)) {
          notes_ = java.util.Collections.unmodifiableList(notes_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.notes_ = notes_;
      } else {
        result.notes_ = notesBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof xyz.chordgen.proto.NoteList) {
        return mergeFrom((xyz.chordgen.proto.NoteList)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(xyz.chordgen.proto.NoteList other) {
      if (other == xyz.chordgen.proto.NoteList.getDefaultInstance()) return this;
      if (notesBuilder_ == null) {
        if (!other.notes_.isEmpty()) {
          if (notes_.isEmpty()) {
            notes_ = other.notes_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureNotesIsMutable();
            notes_.addAll(other.notes_);
          }
          onChanged();
        }
      } else {
        if (!other.notes_.isEmpty()) {
          if (notesBuilder_.isEmpty()) {
            notesBuilder_.dispose();
            notesBuilder_ = null;
            notes_ = other.notes_;
            bitField0_ = (bitField0_ & ~0x00000001);
            notesBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getNotesFieldBuilder() : null;
          } else {
            notesBuilder_.addAllMessages(other.notes_);
          }
        }
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      xyz.chordgen.proto.NoteList parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (xyz.chordgen.proto.NoteList) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.util.List<xyz.chordgen.proto.PlayedNote> notes_ =
      java.util.Collections.emptyList();
    private void ensureNotesIsMutable() {
      if (!((bitField0_ & 0x00000001) == 0x00000001)) {
        notes_ = new java.util.ArrayList<xyz.chordgen.proto.PlayedNote>(notes_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        xyz.chordgen.proto.PlayedNote, xyz.chordgen.proto.PlayedNote.Builder, xyz.chordgen.proto.PlayedNoteOrBuilder> notesBuilder_;

    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public java.util.List<xyz.chordgen.proto.PlayedNote> getNotesList() {
      if (notesBuilder_ == null) {
        return java.util.Collections.unmodifiableList(notes_);
      } else {
        return notesBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public int getNotesCount() {
      if (notesBuilder_ == null) {
        return notes_.size();
      } else {
        return notesBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public xyz.chordgen.proto.PlayedNote getNotes(int index) {
      if (notesBuilder_ == null) {
        return notes_.get(index);
      } else {
        return notesBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder setNotes(
        int index, xyz.chordgen.proto.PlayedNote value) {
      if (notesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureNotesIsMutable();
        notes_.set(index, value);
        onChanged();
      } else {
        notesBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder setNotes(
        int index, xyz.chordgen.proto.PlayedNote.Builder builderForValue) {
      if (notesBuilder_ == null) {
        ensureNotesIsMutable();
        notes_.set(index, builderForValue.build());
        onChanged();
      } else {
        notesBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder addNotes(xyz.chordgen.proto.PlayedNote value) {
      if (notesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureNotesIsMutable();
        notes_.add(value);
        onChanged();
      } else {
        notesBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder addNotes(
        int index, xyz.chordgen.proto.PlayedNote value) {
      if (notesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureNotesIsMutable();
        notes_.add(index, value);
        onChanged();
      } else {
        notesBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder addNotes(
        xyz.chordgen.proto.PlayedNote.Builder builderForValue) {
      if (notesBuilder_ == null) {
        ensureNotesIsMutable();
        notes_.add(builderForValue.build());
        onChanged();
      } else {
        notesBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder addNotes(
        int index, xyz.chordgen.proto.PlayedNote.Builder builderForValue) {
      if (notesBuilder_ == null) {
        ensureNotesIsMutable();
        notes_.add(index, builderForValue.build());
        onChanged();
      } else {
        notesBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder addAllNotes(
        java.lang.Iterable<? extends xyz.chordgen.proto.PlayedNote> values) {
      if (notesBuilder_ == null) {
        ensureNotesIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, notes_);
        onChanged();
      } else {
        notesBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder clearNotes() {
      if (notesBuilder_ == null) {
        notes_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        notesBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public Builder removeNotes(int index) {
      if (notesBuilder_ == null) {
        ensureNotesIsMutable();
        notes_.remove(index);
        onChanged();
      } else {
        notesBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public xyz.chordgen.proto.PlayedNote.Builder getNotesBuilder(
        int index) {
      return getNotesFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public xyz.chordgen.proto.PlayedNoteOrBuilder getNotesOrBuilder(
        int index) {
      if (notesBuilder_ == null) {
        return notes_.get(index);  } else {
        return notesBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public java.util.List<? extends xyz.chordgen.proto.PlayedNoteOrBuilder> 
         getNotesOrBuilderList() {
      if (notesBuilder_ != null) {
        return notesBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(notes_);
      }
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public xyz.chordgen.proto.PlayedNote.Builder addNotesBuilder() {
      return getNotesFieldBuilder().addBuilder(
          xyz.chordgen.proto.PlayedNote.getDefaultInstance());
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public xyz.chordgen.proto.PlayedNote.Builder addNotesBuilder(
        int index) {
      return getNotesFieldBuilder().addBuilder(
          index, xyz.chordgen.proto.PlayedNote.getDefaultInstance());
    }
    /**
     * <code>repeated .chordgen.PlayedNote notes = 1;</code>
     */
    public java.util.List<xyz.chordgen.proto.PlayedNote.Builder> 
         getNotesBuilderList() {
      return getNotesFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        xyz.chordgen.proto.PlayedNote, xyz.chordgen.proto.PlayedNote.Builder, xyz.chordgen.proto.PlayedNoteOrBuilder> 
        getNotesFieldBuilder() {
      if (notesBuilder_ == null) {
        notesBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            xyz.chordgen.proto.PlayedNote, xyz.chordgen.proto.PlayedNote.Builder, xyz.chordgen.proto.PlayedNoteOrBuilder>(
                notes_,
                ((bitField0_ & 0x00000001) == 0x00000001),
                getParentForChildren(),
                isClean());
        notes_ = null;
      }
      return notesBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:chordgen.NoteList)
  }

  // @@protoc_insertion_point(class_scope:chordgen.NoteList)
  private static final xyz.chordgen.proto.NoteList DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new xyz.chordgen.proto.NoteList();
  }

  public static xyz.chordgen.proto.NoteList getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<NoteList>
      PARSER = new com.google.protobuf.AbstractParser<NoteList>() {
    public NoteList parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new NoteList(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<NoteList> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<NoteList> getParserForType() {
    return PARSER;
  }

  public xyz.chordgen.proto.NoteList getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

