// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

/**
 * Protobuf enum {@code chordgen.ChordFlavor}
 */
public enum ChordFlavor
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <code>MAJOR = 0;</code>
   */
  MAJOR(0),
  /**
   * <code>MINOR = 1;</code>
   */
  MINOR(1),
  /**
   * <code>DIMINISHED = 2;</code>
   */
  DIMINISHED(2),
  /**
   * <code>AUGMENTED = 3;</code>
   */
  AUGMENTED(3),
  /**
   * <code>DOMINANT = 4;</code>
   */
  DOMINANT(4),
  /**
   * <code>MAJOR_SEVEN = 5;</code>
   */
  MAJOR_SEVEN(5),
  /**
   * <code>SIXTH = 6;</code>
   */
  SIXTH(6),
  /**
   * <code>EXTENSIONS = 20;</code>
   */
  EXTENSIONS(20),
  UNRECOGNIZED(-1),
  ;

  /**
   * <code>MAJOR = 0;</code>
   */
  public static final int MAJOR_VALUE = 0;
  /**
   * <code>MINOR = 1;</code>
   */
  public static final int MINOR_VALUE = 1;
  /**
   * <code>DIMINISHED = 2;</code>
   */
  public static final int DIMINISHED_VALUE = 2;
  /**
   * <code>AUGMENTED = 3;</code>
   */
  public static final int AUGMENTED_VALUE = 3;
  /**
   * <code>DOMINANT = 4;</code>
   */
  public static final int DOMINANT_VALUE = 4;
  /**
   * <code>MAJOR_SEVEN = 5;</code>
   */
  public static final int MAJOR_SEVEN_VALUE = 5;
  /**
   * <code>SIXTH = 6;</code>
   */
  public static final int SIXTH_VALUE = 6;
  /**
   * <code>EXTENSIONS = 20;</code>
   */
  public static final int EXTENSIONS_VALUE = 20;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static ChordFlavor valueOf(int value) {
    return forNumber(value);
  }

  public static ChordFlavor forNumber(int value) {
    switch (value) {
      case 0: return MAJOR;
      case 1: return MINOR;
      case 2: return DIMINISHED;
      case 3: return AUGMENTED;
      case 4: return DOMINANT;
      case 5: return MAJOR_SEVEN;
      case 6: return SIXTH;
      case 20: return EXTENSIONS;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<ChordFlavor>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      ChordFlavor> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<ChordFlavor>() {
          public ChordFlavor findValueByNumber(int number) {
            return ChordFlavor.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return xyz.chordgen.proto.Proto.getDescriptor()
        .getEnumTypes().get(1);
  }

  private static final ChordFlavor[] VALUES = values();

  public static ChordFlavor valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private ChordFlavor(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:chordgen.ChordFlavor)
}

