// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

/**
 * Protobuf type {@code chordgen.Interval}
 */
public  final class Interval extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:chordgen.Interval)
    IntervalOrBuilder {
  // Use Interval.newBuilder() to construct.
  private Interval(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Interval() {
    range_ = 0;
    sharps_ = 0;
    flats_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private Interval(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 8: {

            range_ = input.readInt32();
            break;
          }
          case 16: {

            sharps_ = input.readInt32();
            break;
          }
          case 24: {

            flats_ = input.readInt32();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_Interval_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_Interval_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            xyz.chordgen.proto.Interval.class, xyz.chordgen.proto.Interval.Builder.class);
  }

  public static final int RANGE_FIELD_NUMBER = 1;
  private int range_;
  /**
   * <code>optional int32 range = 1;</code>
   */
  public int getRange() {
    return range_;
  }

  public static final int SHARPS_FIELD_NUMBER = 2;
  private int sharps_;
  /**
   * <code>optional int32 sharps = 2;</code>
   */
  public int getSharps() {
    return sharps_;
  }

  public static final int FLATS_FIELD_NUMBER = 3;
  private int flats_;
  /**
   * <code>optional int32 flats = 3;</code>
   */
  public int getFlats() {
    return flats_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (range_ != 0) {
      output.writeInt32(1, range_);
    }
    if (sharps_ != 0) {
      output.writeInt32(2, sharps_);
    }
    if (flats_ != 0) {
      output.writeInt32(3, flats_);
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (range_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, range_);
    }
    if (sharps_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(2, sharps_);
    }
    if (flats_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(3, flats_);
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof xyz.chordgen.proto.Interval)) {
      return super.equals(obj);
    }
    xyz.chordgen.proto.Interval other = (xyz.chordgen.proto.Interval) obj;

    boolean result = true;
    result = result && (getRange()
        == other.getRange());
    result = result && (getSharps()
        == other.getSharps());
    result = result && (getFlats()
        == other.getFlats());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptorForType().hashCode();
    hash = (37 * hash) + RANGE_FIELD_NUMBER;
    hash = (53 * hash) + getRange();
    hash = (37 * hash) + SHARPS_FIELD_NUMBER;
    hash = (53 * hash) + getSharps();
    hash = (37 * hash) + FLATS_FIELD_NUMBER;
    hash = (53 * hash) + getFlats();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static xyz.chordgen.proto.Interval parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.Interval parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.Interval parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.Interval parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.Interval parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.Interval parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.Interval parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.Interval parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.Interval parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.Interval parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(xyz.chordgen.proto.Interval prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code chordgen.Interval}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:chordgen.Interval)
      xyz.chordgen.proto.IntervalOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_Interval_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_Interval_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              xyz.chordgen.proto.Interval.class, xyz.chordgen.proto.Interval.Builder.class);
    }

    // Construct using xyz.chordgen.proto.Interval.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      range_ = 0;

      sharps_ = 0;

      flats_ = 0;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_Interval_descriptor;
    }

    public xyz.chordgen.proto.Interval getDefaultInstanceForType() {
      return xyz.chordgen.proto.Interval.getDefaultInstance();
    }

    public xyz.chordgen.proto.Interval build() {
      xyz.chordgen.proto.Interval result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public xyz.chordgen.proto.Interval buildPartial() {
      xyz.chordgen.proto.Interval result = new xyz.chordgen.proto.Interval(this);
      result.range_ = range_;
      result.sharps_ = sharps_;
      result.flats_ = flats_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof xyz.chordgen.proto.Interval) {
        return mergeFrom((xyz.chordgen.proto.Interval)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(xyz.chordgen.proto.Interval other) {
      if (other == xyz.chordgen.proto.Interval.getDefaultInstance()) return this;
      if (other.getRange() != 0) {
        setRange(other.getRange());
      }
      if (other.getSharps() != 0) {
        setSharps(other.getSharps());
      }
      if (other.getFlats() != 0) {
        setFlats(other.getFlats());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      xyz.chordgen.proto.Interval parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (xyz.chordgen.proto.Interval) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int range_ ;
    /**
     * <code>optional int32 range = 1;</code>
     */
    public int getRange() {
      return range_;
    }
    /**
     * <code>optional int32 range = 1;</code>
     */
    public Builder setRange(int value) {
      
      range_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int32 range = 1;</code>
     */
    public Builder clearRange() {
      
      range_ = 0;
      onChanged();
      return this;
    }

    private int sharps_ ;
    /**
     * <code>optional int32 sharps = 2;</code>
     */
    public int getSharps() {
      return sharps_;
    }
    /**
     * <code>optional int32 sharps = 2;</code>
     */
    public Builder setSharps(int value) {
      
      sharps_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int32 sharps = 2;</code>
     */
    public Builder clearSharps() {
      
      sharps_ = 0;
      onChanged();
      return this;
    }

    private int flats_ ;
    /**
     * <code>optional int32 flats = 3;</code>
     */
    public int getFlats() {
      return flats_;
    }
    /**
     * <code>optional int32 flats = 3;</code>
     */
    public Builder setFlats(int value) {
      
      flats_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int32 flats = 3;</code>
     */
    public Builder clearFlats() {
      
      flats_ = 0;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:chordgen.Interval)
  }

  // @@protoc_insertion_point(class_scope:chordgen.Interval)
  private static final xyz.chordgen.proto.Interval DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new xyz.chordgen.proto.Interval();
  }

  public static xyz.chordgen.proto.Interval getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Interval>
      PARSER = new com.google.protobuf.AbstractParser<Interval>() {
    public Interval parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new Interval(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<Interval> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Interval> getParserForType() {
    return PARSER;
  }

  public xyz.chordgen.proto.Interval getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

