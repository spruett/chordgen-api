// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

/**
 * Protobuf type {@code chordgen.ScoringInformation}
 */
public  final class ScoringInformation extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:chordgen.ScoringInformation)
    ScoringInformationOrBuilder {
  // Use ScoringInformation.newBuilder() to construct.
  private ScoringInformation(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private ScoringInformation() {
    score_ = 0D;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private ScoringInformation(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 9: {

            score_ = input.readDouble();
            break;
          }
          case 18: {
            if (!((mutable_bitField0_ & 0x00000002) == 0x00000002)) {
              subscores_ = com.google.protobuf.MapField.newMapField(
                  SubscoresDefaultEntryHolder.defaultEntry);
              mutable_bitField0_ |= 0x00000002;
            }
            com.google.protobuf.MapEntry<java.lang.String, java.lang.Double>
            subscores = input.readMessage(
                SubscoresDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
            subscores_.getMutableMap().put(subscores.getKey(), subscores.getValue());
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_descriptor;
  }

  @SuppressWarnings({"rawtypes"})
  protected com.google.protobuf.MapField internalGetMapField(
      int number) {
    switch (number) {
      case 2:
        return internalGetSubscores();
      default:
        throw new RuntimeException(
            "Invalid map field number: " + number);
    }
  }
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            xyz.chordgen.proto.ScoringInformation.class, xyz.chordgen.proto.ScoringInformation.Builder.class);
  }

  private int bitField0_;
  public static final int SCORE_FIELD_NUMBER = 1;
  private double score_;
  /**
   * <code>optional double score = 1;</code>
   */
  public double getScore() {
    return score_;
  }

  public static final int SUBSCORES_FIELD_NUMBER = 2;
  private static final class SubscoresDefaultEntryHolder {
    static final com.google.protobuf.MapEntry<
        java.lang.String, java.lang.Double> defaultEntry =
            com.google.protobuf.MapEntry
            .<java.lang.String, java.lang.Double>newDefaultInstance(
                xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_SubscoresEntry_descriptor, 
                com.google.protobuf.WireFormat.FieldType.STRING,
                "",
                com.google.protobuf.WireFormat.FieldType.DOUBLE,
                0D);
  }
  private com.google.protobuf.MapField<
      java.lang.String, java.lang.Double> subscores_;
  private com.google.protobuf.MapField<java.lang.String, java.lang.Double>
  internalGetSubscores() {
    if (subscores_ == null) {
      return com.google.protobuf.MapField.emptyMapField(
          SubscoresDefaultEntryHolder.defaultEntry);
    }
    return subscores_;
  }

  public int getSubscoresCount() {
    return internalGetSubscores().getMap().size();
  }
  /**
   * <code>map&lt;string, double&gt; subscores = 2;</code>
   */

  public boolean containsSubscores(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    return internalGetSubscores().getMap().containsKey(key);
  }
  /**
   * Use {@link #getSubscoresMap()} instead.
   */
  @java.lang.Deprecated
  public java.util.Map<java.lang.String, java.lang.Double> getSubscores() {
    return getSubscoresMap();
  }
  /**
   * <code>map&lt;string, double&gt; subscores = 2;</code>
   */

  public java.util.Map<java.lang.String, java.lang.Double> getSubscoresMap() {
    return internalGetSubscores().getMap();
  }
  /**
   * <code>map&lt;string, double&gt; subscores = 2;</code>
   */

  public double getSubscoresOrDefault(
      java.lang.String key,
      double defaultValue) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, java.lang.Double> map =
        internalGetSubscores().getMap();
    return map.containsKey(key) ? map.get(key) : defaultValue;
  }
  /**
   * <code>map&lt;string, double&gt; subscores = 2;</code>
   */

  public double getSubscoresOrThrow(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, java.lang.Double> map =
        internalGetSubscores().getMap();
    if (!map.containsKey(key)) {
      throw new java.lang.IllegalArgumentException();
    }
    return map.get(key);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (score_ != 0D) {
      output.writeDouble(1, score_);
    }
    for (java.util.Map.Entry<java.lang.String, java.lang.Double> entry
         : internalGetSubscores().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, java.lang.Double>
      subscores = SubscoresDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      output.writeMessage(2, subscores);
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (score_ != 0D) {
      size += com.google.protobuf.CodedOutputStream
        .computeDoubleSize(1, score_);
    }
    for (java.util.Map.Entry<java.lang.String, java.lang.Double> entry
         : internalGetSubscores().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, java.lang.Double>
      subscores = SubscoresDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(2, subscores);
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof xyz.chordgen.proto.ScoringInformation)) {
      return super.equals(obj);
    }
    xyz.chordgen.proto.ScoringInformation other = (xyz.chordgen.proto.ScoringInformation) obj;

    boolean result = true;
    result = result && (
        java.lang.Double.doubleToLongBits(getScore())
        == java.lang.Double.doubleToLongBits(
            other.getScore()));
    result = result && internalGetSubscores().equals(
        other.internalGetSubscores());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptorForType().hashCode();
    hash = (37 * hash) + SCORE_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        java.lang.Double.doubleToLongBits(getScore()));
    if (!internalGetSubscores().getMap().isEmpty()) {
      hash = (37 * hash) + SUBSCORES_FIELD_NUMBER;
      hash = (53 * hash) + internalGetSubscores().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.ScoringInformation parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.ScoringInformation parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.ScoringInformation parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(xyz.chordgen.proto.ScoringInformation prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code chordgen.ScoringInformation}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:chordgen.ScoringInformation)
      xyz.chordgen.proto.ScoringInformationOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_descriptor;
    }

    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMapField(
        int number) {
      switch (number) {
        case 2:
          return internalGetSubscores();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMutableMapField(
        int number) {
      switch (number) {
        case 2:
          return internalGetMutableSubscores();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              xyz.chordgen.proto.ScoringInformation.class, xyz.chordgen.proto.ScoringInformation.Builder.class);
    }

    // Construct using xyz.chordgen.proto.ScoringInformation.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      score_ = 0D;

      internalGetMutableSubscores().clear();
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_ScoringInformation_descriptor;
    }

    public xyz.chordgen.proto.ScoringInformation getDefaultInstanceForType() {
      return xyz.chordgen.proto.ScoringInformation.getDefaultInstance();
    }

    public xyz.chordgen.proto.ScoringInformation build() {
      xyz.chordgen.proto.ScoringInformation result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public xyz.chordgen.proto.ScoringInformation buildPartial() {
      xyz.chordgen.proto.ScoringInformation result = new xyz.chordgen.proto.ScoringInformation(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      result.score_ = score_;
      result.subscores_ = internalGetSubscores();
      result.subscores_.makeImmutable();
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof xyz.chordgen.proto.ScoringInformation) {
        return mergeFrom((xyz.chordgen.proto.ScoringInformation)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(xyz.chordgen.proto.ScoringInformation other) {
      if (other == xyz.chordgen.proto.ScoringInformation.getDefaultInstance()) return this;
      if (other.getScore() != 0D) {
        setScore(other.getScore());
      }
      internalGetMutableSubscores().mergeFrom(
          other.internalGetSubscores());
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      xyz.chordgen.proto.ScoringInformation parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (xyz.chordgen.proto.ScoringInformation) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private double score_ ;
    /**
     * <code>optional double score = 1;</code>
     */
    public double getScore() {
      return score_;
    }
    /**
     * <code>optional double score = 1;</code>
     */
    public Builder setScore(double value) {
      
      score_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional double score = 1;</code>
     */
    public Builder clearScore() {
      
      score_ = 0D;
      onChanged();
      return this;
    }

    private com.google.protobuf.MapField<
        java.lang.String, java.lang.Double> subscores_;
    private com.google.protobuf.MapField<java.lang.String, java.lang.Double>
    internalGetSubscores() {
      if (subscores_ == null) {
        return com.google.protobuf.MapField.emptyMapField(
            SubscoresDefaultEntryHolder.defaultEntry);
      }
      return subscores_;
    }
    private com.google.protobuf.MapField<java.lang.String, java.lang.Double>
    internalGetMutableSubscores() {
      onChanged();;
      if (subscores_ == null) {
        subscores_ = com.google.protobuf.MapField.newMapField(
            SubscoresDefaultEntryHolder.defaultEntry);
      }
      if (!subscores_.isMutable()) {
        subscores_ = subscores_.copy();
      }
      return subscores_;
    }

    public int getSubscoresCount() {
      return internalGetSubscores().getMap().size();
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public boolean containsSubscores(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      return internalGetSubscores().getMap().containsKey(key);
    }
    /**
     * Use {@link #getSubscoresMap()} instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.Double> getSubscores() {
      return getSubscoresMap();
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public java.util.Map<java.lang.String, java.lang.Double> getSubscoresMap() {
      return internalGetSubscores().getMap();
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public double getSubscoresOrDefault(
        java.lang.String key,
        double defaultValue) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, java.lang.Double> map =
          internalGetSubscores().getMap();
      return map.containsKey(key) ? map.get(key) : defaultValue;
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public double getSubscoresOrThrow(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, java.lang.Double> map =
          internalGetSubscores().getMap();
      if (!map.containsKey(key)) {
        throw new java.lang.IllegalArgumentException();
      }
      return map.get(key);
    }

    public Builder clearSubscores() {
      getMutableSubscores().clear();
      return this;
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public Builder removeSubscores(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      getMutableSubscores().remove(key);
      return this;
    }
    /**
     * Use alternate mutation accessors instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.Double>
    getMutableSubscores() {
      return internalGetMutableSubscores().getMutableMap();
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */
    public Builder putSubscores(
        java.lang.String key,
        double value) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      
      getMutableSubscores().put(key, value);
      return this;
    }
    /**
     * <code>map&lt;string, double&gt; subscores = 2;</code>
     */

    public Builder putAllSubscores(
        java.util.Map<java.lang.String, java.lang.Double> values) {
      getMutableSubscores().putAll(values);
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:chordgen.ScoringInformation)
  }

  // @@protoc_insertion_point(class_scope:chordgen.ScoringInformation)
  private static final xyz.chordgen.proto.ScoringInformation DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new xyz.chordgen.proto.ScoringInformation();
  }

  public static xyz.chordgen.proto.ScoringInformation getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ScoringInformation>
      PARSER = new com.google.protobuf.AbstractParser<ScoringInformation>() {
    public ScoringInformation parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new ScoringInformation(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ScoringInformation> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ScoringInformation> getParserForType() {
    return PARSER;
  }

  public xyz.chordgen.proto.ScoringInformation getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

