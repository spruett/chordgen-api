// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

/**
 * Protobuf type {@code chordgen.AllChordVoicings}
 */
public  final class AllChordVoicings extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:chordgen.AllChordVoicings)
    AllChordVoicingsOrBuilder {
  // Use AllChordVoicings.newBuilder() to construct.
  private AllChordVoicings(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private AllChordVoicings() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private AllChordVoicings(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              voicings_ = com.google.protobuf.MapField.newMapField(
                  VoicingsDefaultEntryHolder.defaultEntry);
              mutable_bitField0_ |= 0x00000001;
            }
            com.google.protobuf.MapEntry<java.lang.String, xyz.chordgen.proto.VoicingSet>
            voicings = input.readMessage(
                VoicingsDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
            voicings_.getMutableMap().put(voicings.getKey(), voicings.getValue());
            break;
          }
          case 18: {
            if (!((mutable_bitField0_ & 0x00000002) == 0x00000002)) {
              aliases_ = com.google.protobuf.MapField.newMapField(
                  AliasesDefaultEntryHolder.defaultEntry);
              mutable_bitField0_ |= 0x00000002;
            }
            com.google.protobuf.MapEntry<java.lang.String, java.lang.String>
            aliases = input.readMessage(
                AliasesDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
            aliases_.getMutableMap().put(aliases.getKey(), aliases.getValue());
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_descriptor;
  }

  @SuppressWarnings({"rawtypes"})
  protected com.google.protobuf.MapField internalGetMapField(
      int number) {
    switch (number) {
      case 1:
        return internalGetVoicings();
      case 2:
        return internalGetAliases();
      default:
        throw new RuntimeException(
            "Invalid map field number: " + number);
    }
  }
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            xyz.chordgen.proto.AllChordVoicings.class, xyz.chordgen.proto.AllChordVoicings.Builder.class);
  }

  public static final int VOICINGS_FIELD_NUMBER = 1;
  private static final class VoicingsDefaultEntryHolder {
    static final com.google.protobuf.MapEntry<
        java.lang.String, xyz.chordgen.proto.VoicingSet> defaultEntry =
            com.google.protobuf.MapEntry
            .<java.lang.String, xyz.chordgen.proto.VoicingSet>newDefaultInstance(
                xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_VoicingsEntry_descriptor, 
                com.google.protobuf.WireFormat.FieldType.STRING,
                "",
                com.google.protobuf.WireFormat.FieldType.MESSAGE,
                xyz.chordgen.proto.VoicingSet.getDefaultInstance());
  }
  private com.google.protobuf.MapField<
      java.lang.String, xyz.chordgen.proto.VoicingSet> voicings_;
  private com.google.protobuf.MapField<java.lang.String, xyz.chordgen.proto.VoicingSet>
  internalGetVoicings() {
    if (voicings_ == null) {
      return com.google.protobuf.MapField.emptyMapField(
          VoicingsDefaultEntryHolder.defaultEntry);
    }
    return voicings_;
  }

  public int getVoicingsCount() {
    return internalGetVoicings().getMap().size();
  }
  /**
   * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
   */

  public boolean containsVoicings(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    return internalGetVoicings().getMap().containsKey(key);
  }
  /**
   * Use {@link #getVoicingsMap()} instead.
   */
  @java.lang.Deprecated
  public java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> getVoicings() {
    return getVoicingsMap();
  }
  /**
   * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
   */

  public java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> getVoicingsMap() {
    return internalGetVoicings().getMap();
  }
  /**
   * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
   */

  public xyz.chordgen.proto.VoicingSet getVoicingsOrDefault(
      java.lang.String key,
      xyz.chordgen.proto.VoicingSet defaultValue) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> map =
        internalGetVoicings().getMap();
    return map.containsKey(key) ? map.get(key) : defaultValue;
  }
  /**
   * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
   */

  public xyz.chordgen.proto.VoicingSet getVoicingsOrThrow(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> map =
        internalGetVoicings().getMap();
    if (!map.containsKey(key)) {
      throw new java.lang.IllegalArgumentException();
    }
    return map.get(key);
  }

  public static final int ALIASES_FIELD_NUMBER = 2;
  private static final class AliasesDefaultEntryHolder {
    static final com.google.protobuf.MapEntry<
        java.lang.String, java.lang.String> defaultEntry =
            com.google.protobuf.MapEntry
            .<java.lang.String, java.lang.String>newDefaultInstance(
                xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_AliasesEntry_descriptor, 
                com.google.protobuf.WireFormat.FieldType.STRING,
                "",
                com.google.protobuf.WireFormat.FieldType.STRING,
                "");
  }
  private com.google.protobuf.MapField<
      java.lang.String, java.lang.String> aliases_;
  private com.google.protobuf.MapField<java.lang.String, java.lang.String>
  internalGetAliases() {
    if (aliases_ == null) {
      return com.google.protobuf.MapField.emptyMapField(
          AliasesDefaultEntryHolder.defaultEntry);
    }
    return aliases_;
  }

  public int getAliasesCount() {
    return internalGetAliases().getMap().size();
  }
  /**
   * <code>map&lt;string, string&gt; aliases = 2;</code>
   */

  public boolean containsAliases(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    return internalGetAliases().getMap().containsKey(key);
  }
  /**
   * Use {@link #getAliasesMap()} instead.
   */
  @java.lang.Deprecated
  public java.util.Map<java.lang.String, java.lang.String> getAliases() {
    return getAliasesMap();
  }
  /**
   * <code>map&lt;string, string&gt; aliases = 2;</code>
   */

  public java.util.Map<java.lang.String, java.lang.String> getAliasesMap() {
    return internalGetAliases().getMap();
  }
  /**
   * <code>map&lt;string, string&gt; aliases = 2;</code>
   */

  public java.lang.String getAliasesOrDefault(
      java.lang.String key,
      java.lang.String defaultValue) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, java.lang.String> map =
        internalGetAliases().getMap();
    return map.containsKey(key) ? map.get(key) : defaultValue;
  }
  /**
   * <code>map&lt;string, string&gt; aliases = 2;</code>
   */

  public java.lang.String getAliasesOrThrow(
      java.lang.String key) {
    if (key == null) { throw new java.lang.NullPointerException(); }
    java.util.Map<java.lang.String, java.lang.String> map =
        internalGetAliases().getMap();
    if (!map.containsKey(key)) {
      throw new java.lang.IllegalArgumentException();
    }
    return map.get(key);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (java.util.Map.Entry<java.lang.String, xyz.chordgen.proto.VoicingSet> entry
         : internalGetVoicings().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, xyz.chordgen.proto.VoicingSet>
      voicings = VoicingsDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      output.writeMessage(1, voicings);
    }
    for (java.util.Map.Entry<java.lang.String, java.lang.String> entry
         : internalGetAliases().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, java.lang.String>
      aliases = AliasesDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      output.writeMessage(2, aliases);
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (java.util.Map.Entry<java.lang.String, xyz.chordgen.proto.VoicingSet> entry
         : internalGetVoicings().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, xyz.chordgen.proto.VoicingSet>
      voicings = VoicingsDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(1, voicings);
    }
    for (java.util.Map.Entry<java.lang.String, java.lang.String> entry
         : internalGetAliases().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, java.lang.String>
      aliases = AliasesDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(2, aliases);
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof xyz.chordgen.proto.AllChordVoicings)) {
      return super.equals(obj);
    }
    xyz.chordgen.proto.AllChordVoicings other = (xyz.chordgen.proto.AllChordVoicings) obj;

    boolean result = true;
    result = result && internalGetVoicings().equals(
        other.internalGetVoicings());
    result = result && internalGetAliases().equals(
        other.internalGetAliases());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptorForType().hashCode();
    if (!internalGetVoicings().getMap().isEmpty()) {
      hash = (37 * hash) + VOICINGS_FIELD_NUMBER;
      hash = (53 * hash) + internalGetVoicings().hashCode();
    }
    if (!internalGetAliases().getMap().isEmpty()) {
      hash = (37 * hash) + ALIASES_FIELD_NUMBER;
      hash = (53 * hash) + internalGetAliases().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static xyz.chordgen.proto.AllChordVoicings parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(xyz.chordgen.proto.AllChordVoicings prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code chordgen.AllChordVoicings}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:chordgen.AllChordVoicings)
      xyz.chordgen.proto.AllChordVoicingsOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_descriptor;
    }

    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMapField(
        int number) {
      switch (number) {
        case 1:
          return internalGetVoicings();
        case 2:
          return internalGetAliases();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMutableMapField(
        int number) {
      switch (number) {
        case 1:
          return internalGetMutableVoicings();
        case 2:
          return internalGetMutableAliases();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              xyz.chordgen.proto.AllChordVoicings.class, xyz.chordgen.proto.AllChordVoicings.Builder.class);
    }

    // Construct using xyz.chordgen.proto.AllChordVoicings.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      internalGetMutableVoicings().clear();
      internalGetMutableAliases().clear();
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return xyz.chordgen.proto.Proto.internal_static_chordgen_AllChordVoicings_descriptor;
    }

    public xyz.chordgen.proto.AllChordVoicings getDefaultInstanceForType() {
      return xyz.chordgen.proto.AllChordVoicings.getDefaultInstance();
    }

    public xyz.chordgen.proto.AllChordVoicings build() {
      xyz.chordgen.proto.AllChordVoicings result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public xyz.chordgen.proto.AllChordVoicings buildPartial() {
      xyz.chordgen.proto.AllChordVoicings result = new xyz.chordgen.proto.AllChordVoicings(this);
      int from_bitField0_ = bitField0_;
      result.voicings_ = internalGetVoicings();
      result.voicings_.makeImmutable();
      result.aliases_ = internalGetAliases();
      result.aliases_.makeImmutable();
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof xyz.chordgen.proto.AllChordVoicings) {
        return mergeFrom((xyz.chordgen.proto.AllChordVoicings)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(xyz.chordgen.proto.AllChordVoicings other) {
      if (other == xyz.chordgen.proto.AllChordVoicings.getDefaultInstance()) return this;
      internalGetMutableVoicings().mergeFrom(
          other.internalGetVoicings());
      internalGetMutableAliases().mergeFrom(
          other.internalGetAliases());
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      xyz.chordgen.proto.AllChordVoicings parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (xyz.chordgen.proto.AllChordVoicings) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private com.google.protobuf.MapField<
        java.lang.String, xyz.chordgen.proto.VoicingSet> voicings_;
    private com.google.protobuf.MapField<java.lang.String, xyz.chordgen.proto.VoicingSet>
    internalGetVoicings() {
      if (voicings_ == null) {
        return com.google.protobuf.MapField.emptyMapField(
            VoicingsDefaultEntryHolder.defaultEntry);
      }
      return voicings_;
    }
    private com.google.protobuf.MapField<java.lang.String, xyz.chordgen.proto.VoicingSet>
    internalGetMutableVoicings() {
      onChanged();;
      if (voicings_ == null) {
        voicings_ = com.google.protobuf.MapField.newMapField(
            VoicingsDefaultEntryHolder.defaultEntry);
      }
      if (!voicings_.isMutable()) {
        voicings_ = voicings_.copy();
      }
      return voicings_;
    }

    public int getVoicingsCount() {
      return internalGetVoicings().getMap().size();
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public boolean containsVoicings(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      return internalGetVoicings().getMap().containsKey(key);
    }
    /**
     * Use {@link #getVoicingsMap()} instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> getVoicings() {
      return getVoicingsMap();
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> getVoicingsMap() {
      return internalGetVoicings().getMap();
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public xyz.chordgen.proto.VoicingSet getVoicingsOrDefault(
        java.lang.String key,
        xyz.chordgen.proto.VoicingSet defaultValue) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> map =
          internalGetVoicings().getMap();
      return map.containsKey(key) ? map.get(key) : defaultValue;
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public xyz.chordgen.proto.VoicingSet getVoicingsOrThrow(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> map =
          internalGetVoicings().getMap();
      if (!map.containsKey(key)) {
        throw new java.lang.IllegalArgumentException();
      }
      return map.get(key);
    }

    public Builder clearVoicings() {
      getMutableVoicings().clear();
      return this;
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public Builder removeVoicings(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      getMutableVoicings().remove(key);
      return this;
    }
    /**
     * Use alternate mutation accessors instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet>
    getMutableVoicings() {
      return internalGetMutableVoicings().getMutableMap();
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */
    public Builder putVoicings(
        java.lang.String key,
        xyz.chordgen.proto.VoicingSet value) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      if (value == null) { throw new java.lang.NullPointerException(); }
      getMutableVoicings().put(key, value);
      return this;
    }
    /**
     * <code>map&lt;string, .chordgen.VoicingSet&gt; voicings = 1;</code>
     */

    public Builder putAllVoicings(
        java.util.Map<java.lang.String, xyz.chordgen.proto.VoicingSet> values) {
      getMutableVoicings().putAll(values);
      return this;
    }

    private com.google.protobuf.MapField<
        java.lang.String, java.lang.String> aliases_;
    private com.google.protobuf.MapField<java.lang.String, java.lang.String>
    internalGetAliases() {
      if (aliases_ == null) {
        return com.google.protobuf.MapField.emptyMapField(
            AliasesDefaultEntryHolder.defaultEntry);
      }
      return aliases_;
    }
    private com.google.protobuf.MapField<java.lang.String, java.lang.String>
    internalGetMutableAliases() {
      onChanged();;
      if (aliases_ == null) {
        aliases_ = com.google.protobuf.MapField.newMapField(
            AliasesDefaultEntryHolder.defaultEntry);
      }
      if (!aliases_.isMutable()) {
        aliases_ = aliases_.copy();
      }
      return aliases_;
    }

    public int getAliasesCount() {
      return internalGetAliases().getMap().size();
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public boolean containsAliases(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      return internalGetAliases().getMap().containsKey(key);
    }
    /**
     * Use {@link #getAliasesMap()} instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.String> getAliases() {
      return getAliasesMap();
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public java.util.Map<java.lang.String, java.lang.String> getAliasesMap() {
      return internalGetAliases().getMap();
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public java.lang.String getAliasesOrDefault(
        java.lang.String key,
        java.lang.String defaultValue) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, java.lang.String> map =
          internalGetAliases().getMap();
      return map.containsKey(key) ? map.get(key) : defaultValue;
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public java.lang.String getAliasesOrThrow(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      java.util.Map<java.lang.String, java.lang.String> map =
          internalGetAliases().getMap();
      if (!map.containsKey(key)) {
        throw new java.lang.IllegalArgumentException();
      }
      return map.get(key);
    }

    public Builder clearAliases() {
      getMutableAliases().clear();
      return this;
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public Builder removeAliases(
        java.lang.String key) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      getMutableAliases().remove(key);
      return this;
    }
    /**
     * Use alternate mutation accessors instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.String>
    getMutableAliases() {
      return internalGetMutableAliases().getMutableMap();
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */
    public Builder putAliases(
        java.lang.String key,
        java.lang.String value) {
      if (key == null) { throw new java.lang.NullPointerException(); }
      if (value == null) { throw new java.lang.NullPointerException(); }
      getMutableAliases().put(key, value);
      return this;
    }
    /**
     * <code>map&lt;string, string&gt; aliases = 2;</code>
     */

    public Builder putAllAliases(
        java.util.Map<java.lang.String, java.lang.String> values) {
      getMutableAliases().putAll(values);
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:chordgen.AllChordVoicings)
  }

  // @@protoc_insertion_point(class_scope:chordgen.AllChordVoicings)
  private static final xyz.chordgen.proto.AllChordVoicings DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new xyz.chordgen.proto.AllChordVoicings();
  }

  public static xyz.chordgen.proto.AllChordVoicings getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<AllChordVoicings>
      PARSER = new com.google.protobuf.AbstractParser<AllChordVoicings>() {
    public AllChordVoicings parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new AllChordVoicings(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<AllChordVoicings> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<AllChordVoicings> getParserForType() {
    return PARSER;
  }

  public xyz.chordgen.proto.AllChordVoicings getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

