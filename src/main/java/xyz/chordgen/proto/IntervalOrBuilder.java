// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: chordgen.proto

package xyz.chordgen.proto;

public interface IntervalOrBuilder extends
    // @@protoc_insertion_point(interface_extends:chordgen.Interval)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional int32 range = 1;</code>
   */
  int getRange();

  /**
   * <code>optional int32 sharps = 2;</code>
   */
  int getSharps();

  /**
   * <code>optional int32 flats = 3;</code>
   */
  int getFlats();
}
