package xyz.chordgen

import io.kotlintest.matchers.numerics.shouldBeLessThan
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.Tunings

class TuningTest : StringSpec({
    "standard tuning is ascending in pitch" {
        Tunings.standardTuning.strings.zipWithNext().forEach { (a, b) ->
            a.openPitch.semitone.shouldBeLessThan(b.openPitch.semitone)
        }
    }
})