package xyz.chordgen

import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.Fingering
import xyz.chordgen.lib.ScoringInformation
import xyz.chordgen.lib.ScoringOptions
import xyz.chordgen.lib.Tuning
import xyz.chordgen.lib.Tunings
import xyz.chordgen.lib.Voicing
import xyz.chordgen.lib.getFingerings

private val testOpts = ScoringOptions()

private fun isFingeringPlayable(voicing: Voicing, tuning: Tuning = Tunings.standardTuning): Boolean {
    return getFingerings(voicing, tuning, testOpts.fingering).isNotEmpty()
}

private fun bestFingering(
    voicing: Voicing,
    tuning: Tuning = Tunings.standardTuning
): Pair<Fingering, ScoringInformation> {
    return getFingerings(voicing, tuning, testOpts.fingering).first()
}

class PlayabilityTest : StringSpec({
    "basic Voicing check" {
        val good = makeVoicing("C", "-3201-")
        good.playable().shouldBeTrue()

        val tooFar = makeVoicing("F", "187211")
        tooFar.playable().shouldBeFalse()

        val allSame = makeVoicing("G", "3--0-3")
        allSame.playable().shouldBeFalse()
    }
    "fingering check" {
        val good = listOf(
            makeVoicing("C", "-3201-"),
            makeVoicing("G", "320003"),
            makeVoicing("F#", "244322"),
            makeVoicing("D", "---232")
        )
        good.forEach {
            isFingeringPlayable(it).shouldBeTrue()
        }

        val bad = listOf(
            makeVoicing("G", "325433"),
            makeVoicing("F#m9", "247222")
        )
        bad.forEach {
            isFingeringPlayable(it).shouldBeFalse()
        }
    }
})