package xyz.chordgen

import io.kotlintest.matchers.collections.shouldBeEmpty
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.Chord
import xyz.chordgen.lib.ChordNote
import xyz.chordgen.lib.ChordNoteType
import xyz.chordgen.lib.Note

class ChordTest : StringSpec({
    "parses common major chords" {
        val cmaj = Chord.fromString("C")
        cmaj.notes.shouldBe(any = listOf(
                ChordNote(Note.fromString("C"), ChordNoteType.ROOT),
                ChordNote(Note.fromString("E"), ChordNoteType.THIRD),
                ChordNote(Note.fromString("G"), ChordNoteType.FIFTH)
        ))
        cmaj.getBass().shouldBe(null)
        cmaj.getRoot().note.shouldBe(Note.fromString("C"))
        cmaj.getThird().note.shouldBe(Note.fromString("E"))
        cmaj.getFifth().note.shouldBe(Note.fromString("G"))
        cmaj.getExtensions().shouldBeEmpty()

        val ebmaj = Chord.fromString("Eb")
        ebmaj.notes.shouldBe(listOf(
                ChordNote(Note.fromString("Eb"), ChordNoteType.ROOT),
                ChordNote(Note.fromString("G"), ChordNoteType.THIRD),
                ChordNote(Note.fromString("Bb"), ChordNoteType.FIFTH)
        ))
    }
    "parses extension chords" {
        val cm9 = Chord.fromString("Cm9")
        cm9.notes.shouldBe(any = listOf(
                ChordNote(Note.fromString("C"), ChordNoteType.ROOT),
                ChordNote(Note.fromString("Eb"), ChordNoteType.THIRD),
                ChordNote(Note.fromString("G"), ChordNoteType.FIFTH),
                ChordNote(Note.fromString("Bb"), ChordNoteType.SEVENTH),
                ChordNote(Note.fromString("D"), ChordNoteType.EXTENSION, explicit = true)
        ))
    }
    "parses chords with bass" {
        val db = Chord.fromString("DbM7/E")
        db.notes.shouldBe(any = listOf(
                ChordNote(Note.fromString("E"), ChordNoteType.BASS, explicit = true),
                ChordNote(Note.fromString("Db"), ChordNoteType.ROOT),
                ChordNote(Note.fromString("F"), ChordNoteType.THIRD),
                ChordNote(Note.fromString("Ab"), ChordNoteType.FIFTH),
                ChordNote(Note.fromString("C"), ChordNoteType.SEVENTH, explicit = true)
        ))
    }
})