package xyz.chordgen

import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.matchers.collections.shouldContain
import io.kotlintest.matchers.numerics.shouldBeBetween
import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.Chord
import xyz.chordgen.lib.Note
import xyz.chordgen.lib.Tuning
import xyz.chordgen.lib.Tunings
import xyz.chordgen.lib.Voicing
import xyz.chordgen.lib.findVoicings

fun makeVoicing(name: String, fretting: String, tuning: Tuning = Tunings.standardTuning): Voicing {
    val frets = fretting.map {
        if (Character.isDigit(it)) {
            Integer.parseInt(it.toString())
        } else {
            null
        }
    }
    return Voicing.fromFrets(frets, chord = Chord.fromString(name), tuning = tuning)
}

class VoicingTest : StringSpec({
    "gets common voicings for C" {
        val voicings = findVoicings(Chord.fromString("C"), Tunings.standardTuning)
        voicings.size.shouldBeGreaterThan(100)

        val commonVoicings = listOf(
                makeVoicing("C", "-32010"),
                makeVoicing("C", "-3201-"),
                makeVoicing("C", "-35553")
        )
        for (voicing in commonVoicings) {
            voicings.shouldContain(voicing)
        }

        for (voicing in voicings) {
            voicing.notes.size.shouldBeBetween(3, 6)
            voicing.playable().shouldBeTrue()

            voicing.notes.map { it.note }.forEach {
                val notes = setOf(
                        Note.fromString("C"),
                        Note.fromString("E"),
                        Note.fromString("G")
                )
                notes.shouldContain(it)
            }
        }
    }

    "gets common voicings for Db7" {
        val voicings = findVoicings(Chord.fromString("Db7"), Tunings.standardTuning)
        voicings.size.shouldBeGreaterThan(100)

        val commonVoicings = listOf(
                makeVoicing("Db7", "-434--"),
                makeVoicing("Db7", "-46464")
        )
        for (voicing in commonVoicings) {
            voicings.shouldContain(voicing)
        }

        for (voicing in voicings) {
            voicing.notes.size.shouldBeBetween(3, 6)
            voicing.playable().shouldBeTrue()

            voicing.notes.map { it.note }.forEach {
                val notes = setOf(
                        Note.fromString("Db"),
                        Note.fromString("F"),
                        Note.fromString("Ab"),
                        Note.fromString("Cb")
                )
                notes.shouldContain(it)
            }
        }
    }
})