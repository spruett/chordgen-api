package xyz.chordgen

import io.kotlintest.Description
import io.kotlintest.matchers.numerics.shouldBeGreaterThan
import io.kotlintest.matchers.numerics.shouldBeLessThanOrEqual
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.matchers.string.shouldNotBeEmpty
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.restassured.RestAssured.get
import io.restassured.RestAssured.given
import xyz.chordgen.api.ApiServer
import xyz.chordgen.proto.Chord
import xyz.chordgen.proto.ChordFlavor
import xyz.chordgen.proto.Error
import xyz.chordgen.proto.Settings
import xyz.chordgen.proto.TuningId
import xyz.chordgen.proto.VoicingSet
import java.util.Base64

class ApiTest : StringSpec() {
    private val app = ApiServer()
    override fun beforeTest(description: Description) {
        if (!app.isStarted) {
            app.start("server.join=false")
        }
    }
    fun getVoicings(name: String): VoicingSet {
        val resp = get("/voicings/?name=$name")
        resp.statusCode.shouldBe(200)
        return VoicingSet.parseFrom(resp.body.asInputStream().readAllBytes())
    }

    init {
        "notes endpoint protobuf" {
            val resp = get("/notes/?name=C")
            resp.then().assertThat().statusCode(200)
            resp.contentType.shouldBe("application/x-protobuf")

            val message = Chord.parseFrom(resp.body.asInputStream().readAllBytes())
            message.name.shouldBe("C")
            message.notesList.map { it.name }.shouldBe(listOf("C", "E", "G"))
        }
        "voicings endpoint protobuf" {
            val resp = get("/voicings/?name=C")
            resp.then().assertThat().statusCode(200)
            resp.contentType.shouldBe("application/x-protobuf")

            val message = VoicingSet.parseFrom(resp.body.asInputStream().readAllBytes())
            message.voicingsCount.shouldBeGreaterThan(10)
            message.chord.name.shouldBe("C")
            message.tuning.id.shouldBe(TuningId.STANDARD)
            message.tuning.capo.shouldBe(0)
            message.tuning.stringsCount.shouldBe(6)
        }

        "error handling" {
            val resp = get("/voicings/?name=bad")
            resp.then().assertThat().statusCode(400)
            resp.contentType.shouldBe("application/x-protobuf")

            val message = Error.parseFrom(resp.body.asInputStream().readAllBytes())
            message.message.shouldNotBeEmpty()
        }

        "json" {
            val resp = given().header("Accept", "application/json")
                    .get("/notes/?name=C")
            resp.then().assertThat().statusCode(200)
            resp.contentType.shouldContain("application/json")
            resp.body.jsonPath().getString("name").shouldBe("C")

            val error = given().header("Accept", "application/json")
                    .get("/notes/?name=bad")
            error.then().assertThat().statusCode(400)
            error.contentType.shouldContain("application/json")
            error.body.jsonPath().getString("message").shouldNotBeEmpty()
        }

        "chord flavors" {
            val c7 = getVoicings("C7")
            c7.chord.flavorsList.shouldBe(listOf(
                ChordFlavor.MAJOR,
                ChordFlavor.DOMINANT
            ))

            val bb6 = getVoicings("Bb6")
            bb6.chord.flavorsList.shouldBe(listOf(
                ChordFlavor.MAJOR,
                ChordFlavor.SIXTH
            ))

            val am9 = getVoicings("Am9")
            am9.chord.flavorsList.shouldBe(listOf(
                ChordFlavor.MINOR,
                ChordFlavor.DOMINANT,
                ChordFlavor.EXTENSIONS
            ))
        }

        "chord intervals" {
            val em7 = getVoicings("Em7")
            val intervals = em7.chord.notesList.map { it.interval }
            intervals.map { Triple(it.range, it.sharps, it.flats) }.shouldBe(
                listOf(
                    Triple(1, 0, 0),
                    Triple(3, 0, 1),
                    Triple(5, 0, 0),
                    Triple(7, 0, 1)
                )
            )
        }
        "uke tuning from settings" {
            val settings = Settings.newBuilder()
                .setTuning(TuningId.UKULELE)
                .build()
            val encoded = String(Base64.getEncoder().encode(settings.toByteArray()))
            val voicings = getVoicings("C&settings=$encoded")

            voicings.tuning.id.shouldBe(TuningId.UKULELE)
            voicings.tuning.stringsCount.shouldBe(4)
            voicings.voicingsList.forEach { voicing ->
                voicing.notesList.size.shouldBeLessThanOrEqual(4)
                voicing.notesList.size.shouldBeGreaterThan(1)
            }
        }
    }
}