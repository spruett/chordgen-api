package xyz.chordgen

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.DedupOptions
import xyz.chordgen.lib.takeNonSimilarVoicings

class DedupTest : StringSpec({
    val tallBar = makeVoicing("F#m", "244222")
    val almost = makeVoicing("F#m", "24422-")
    val other = makeVoicing("F#m", "-44675")

    "simple dedup voicings" {
        val deduped = takeNonSimilarVoicings(3, listOf(tallBar, almost, other), { it }, DedupOptions())
        deduped.shouldBe(listOf(tallBar, other))
    }
})