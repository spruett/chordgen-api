package xyz.chordgen

import io.kotlintest.matchers.plusOrMinus
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.Note
import xyz.chordgen.lib.Pitch

class PitchTest : StringSpec({
    "parses common pitches" {
        val pitch = Pitch.fromNoteAndOctave(Note.fromString("A"), 4)
        pitch.octave().shouldBe(4)
        pitch.semitoneOffset().shouldBe(9)
        pitch.toString().shouldBe("Pitch(A_4)")
    }
    "frequency calculation" {
        Pitch.fromNoteAndOctave(Note.fromString("A"), 4)
            .frequency().shouldBe(440.0 plusOrMinus 1.0)
        Pitch.fromNoteAndOctave(Note.fromString("A"), 5)
            .frequency().shouldBe(880.0 plusOrMinus 1.0)
        Pitch.fromNoteAndOctave(Note.fromString("C"), 3)
            .frequency().shouldBe(131.0 plusOrMinus 1.0)
    }
})