package xyz.chordgen

import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import xyz.chordgen.lib.util.BoundedSolutionList

class BoundedSolutionListTest : StringSpec({
    "adds elements" {
        val set = BoundedSolutionList<Int>(5, Comparator { o1, o2 -> o1 - o2 })
        set.isFull().shouldBeFalse()

        set.add(1)
        set.isFull().shouldBeFalse()
        set.toList().shouldBe(listOf(1))

        set.add(3)
        set.add(2)
        set.isFull().shouldBeFalse()
        set.toList().shouldBe(listOf(1, 2, 3))
        set.peek().shouldBe(1)

        set.add(5)
        set.add(10)
        set.isFull().shouldBeTrue()
        set.toList().shouldBe(listOf(1, 2, 3, 5, 10))

        set.add(0)
        set.isFull().shouldBeTrue()
        set.toList().shouldBe(listOf(1, 2, 3, 5, 10))

        set.add(11)
        set.isFull().shouldBeTrue()
        set.toList().shouldBe(listOf(2, 3, 5, 10, 11))
    }
    "compact heap" {
        val set = BoundedSolutionList<Int>(5, Comparator { o1, o2 -> o1 - o2 })
        set.add(1)
        set.compact { list -> list.filter { it < 3 } }

        set.toList().shouldBe(listOf(1))

        set.add(3)
        set.add(5)
        set.add(10)
        set.add(11)
        set.toList().shouldBe(listOf(1, 3, 5, 10, 11))

        set.compact { list -> list.filter { it <= 5 } }
        set.toList().shouldBe(listOf(1, 3, 5))
    }
})